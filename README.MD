# castlewar-session-server

Is still under development.

Following information is for the developers.

## Build

```bash
mvn package
```

## Test

```bash
mvn jacoco:prepare-agent test jacoco:report sonar:sonar -Dsonar.projectKey=castlewar-session-server -Dsonar.host.url=http://localhost:9000 -Dsonar.login=<secret>
```

## Requirements

 * Java 15
