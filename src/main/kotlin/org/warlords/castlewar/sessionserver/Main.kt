package org.warlords.castlewar.sessionserver

import com.google.protobuf.InvalidProtocolBufferException
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import org.slf4j.LoggerFactory
import org.warlords.castlewar.proto.Castlewar
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.entities.EntityTypeId
import org.warlords.castlewar.sessionserver.ecs.entities.buildings.*
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.net.CastleWarSessionServer
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER
import org.warlords.castlewar.sessionserver.utility.Vector
import org.warlords.castlewar.sessionserver.utility.repr
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.scheduleAtFixedRate
import kotlin.reflect.KClass
import kotlin.system.measureTimeMillis

const val DEFAULT_PORT = 58889

const val CELL_SIZE = 50.0

const val WIDTH = CELL_SIZE * 24.0
const val HEIGHT = CELL_SIZE * 12.0

const val UPDATE_PERIOD = 50

val logger = LoggerFactory.getLogger("server")!!

fun startEngine(timer: Timer, engine: Engine) {
    var elapsedTime = 0
    timer.scheduleAtFixedRate(0, UPDATE_PERIOD.toLong()) {
        val time = measureTimeMillis {
            engine.update(elapsedTime)
        }
        if (time > UPDATE_PERIOD.toLong()) {
            logger.debug("Update cycle took $time ms!")
        }
        elapsedTime += UPDATE_PERIOD
    }
}

fun buildingClass(typeId: Int): KClass<out Building>? = when (typeId) {
    EntityTypeId["BomberAirfield"] -> BomberAirfield::class
    EntityTypeId["FighterAirfield"] -> FighterAirfield::class
    EntityTypeId["TankFactory"] -> TankFactory::class
    EntityTypeId["LightTankFactory"] -> LightTankFactory::class
    EntityTypeId["SAM"] -> SAM::class
    EntityTypeId["Artillery"] -> Artillery::class
    EntityTypeId["AirshipFactory"] -> AirshipFactory::class
    else -> null
}

fun main(args: Array<String>) {
    val parser = ArgParser("castlewar-session-server")
    val port by parser.option(ArgType.Int, "port", "p", "port to run on").default(DEFAULT_PORT)
    val initialMinerals by parser.option(
        ArgType.Int, "minerals", "m", "initial minerals count for both players"
    ).default(200 + Building.buildCost[HQ::class]!!.minerals)
    val initialTerrazine by parser.option(
        ArgType.Int, "terrazine", "t", "initial terrazine count for both players"
    ).default(200)
    parser.parse(args)

    val server = CastleWarSessionServer(port)

    val engine = Engine(
        Engine.DEFAULT_UPDATE_PERIOD,
        AttackSystem(),
        BuildingPlacementSystem(WIDTH, HEIGHT, CELL_SIZE),
        BulletSystem(),
        GroundMovementSystem(WIDTH, HEIGHT),
        TurretBindSystem(),
        CollisionSystem(),
        EconomicsSystem(initialMinerals, initialTerrazine, initialMinerals, initialTerrazine),
        DeathSystem(),
        HealthRegenSystem(),
        LifetimeSystem(),
        LineOfSightTargetSystem(Vector(WIDTH, HEIGHT), Vector(WIDTH / 2, HEIGHT / 2)),
        SeekerSystem(),
        SerializationSystem { state ->
            val message = Castlewar.ServerMessage.newBuilder()
                .setState(state)
                .build()
                .toByteArray()
            server.send(LEFT_PLAYER, message)
            server.send(RIGHT_PLAYER, message)
        },
        UnitSpawnSystem()
    )

    val buildSystem = engine.get(BuildingPlacementSystem::class)!!
    val economicsSystem = engine.get(EconomicsSystem::class)!!

    buildSystem.apply {
        placeBuilding(HQ::class, Vector(180.0, HEIGHT / 2), LEFT_PLAYER)
        placeBuilding(HQ::class, Vector(WIDTH - 180.0, HEIGHT / 2), RIGHT_PLAYER)
    }

    val engineTimer = Timer(true)
    val running = AtomicBoolean(false)

    server.apply {
        onConnected { player ->
            logger.info("${player.repr()} connected")

            if (bothPlayersConnected() && !running.getAndSet(true)) {
                send(
                    LEFT_PLAYER, Castlewar.ServerMessage.newBuilder()
                        .setReady(
                            Castlewar.Ready.newBuilder()
                                .setPlayer(LEFT_PLAYER)
                                .build()
                        )
                        .build()
                        .toByteArray()
                )
                send(
                    RIGHT_PLAYER, Castlewar.ServerMessage.newBuilder()
                        .setReady(
                            Castlewar.Ready.newBuilder()
                                .setPlayer(RIGHT_PLAYER)
                                .build()
                        )
                        .build()
                        .toByteArray()
                )
                startEngine(engineTimer, engine)
            }
        }
        onDisconnected { player ->
            logger.info("${player.repr()} disconnected")

            if (neitherPlayerConnected()) {
                logger.info("Neither player is connected. Stopping...")
                engine.runLater {
                    engineTimer.cancel()
                    server.stop()
                }
            }
        }
        onMessage { player, message ->
            try {
                val clientMessage = Castlewar.ClientMessage.parseFrom(message)
                when {
                    clientMessage.hasBuild() -> {
                        val build = clientMessage.build
                        val at = Vector(build.x, build.y)
                        val building = buildingClass(build.typeId)

                        logger.info("${player.repr()} wants to place a building at $at")

                        building?.let {
                            engine.runLater {
                                val result = buildSystem.placeBuilding(building, at, player)
                                val response = Castlewar.ServerMessage.newBuilder()
                                    .setBuildResult(
                                        Castlewar.BuildResult.newBuilder()
                                            .setResult(result)
                                            .build()
                                    )
                                    .build()
                                    .toByteArray()
                                send(player, response)
                            }
                        }
                    }
                    clientMessage.hasSell() -> {
                        val sell = clientMessage.sell
                        logger.info("${player.repr()} wants to sell a building at (${sell.x}, ${sell.y})")
                        engine.runLater {
                            val building = buildSystem.getBuilding(sell.x, sell.y)
                            building?.let {
                                val healthComponent = it.get(HealthComponent::class)!!
                                val healthPercentage = healthComponent.current / healthComponent.max
                                healthComponent.current = 0

                                val buildCost = Building.buildCost[building::class]!!
                                economicsSystem.run {
                                    minerals[player] = (minerals[player] ?: 0) +
                                            (buildCost.minerals * healthPercentage * 0.8).toInt()
                                    terrazine[player] = (terrazine[player] ?: 0) +
                                            (buildCost.terrazine * healthPercentage * 0.8).toInt()
                                }
                                logger.info("building at (${sell.x}, ${sell.y}) sold")
                            }
                        }
                    }
                }
            } catch (e: InvalidProtocolBufferException) {
                logger.warn("${player.repr()} sent invalid data: ${e.message}")
            }
        }
    }

    server.start()
}
