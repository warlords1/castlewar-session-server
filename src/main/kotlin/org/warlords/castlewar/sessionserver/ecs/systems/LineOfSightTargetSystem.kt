package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.utility.Vector
import org.warlords.castlewar.sessionserver.utility.sq
import kotlin.math.PI

class LineOfSightTargetSystem(private val bounds: Vector, private val returnTo: Vector) : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (transform, target, lineOfSight, affiliation) = entity.getEnabled(
                TransformComponent::class, TargetComponent::class, LineOfSightComponent::class,
                AffiliationComponent::class
            ) ?: continue

            if (engine.exists(target.id)) {
                if (target.lockType == LockType.LineOfSight
                    && !transform.hasLineOfSight(lineOfSight, target.current)
                ) {
                    target.id = Entity.NO_ID
                    target.current = target.current + transform.direction * target.distanceBuffer
                }
            } else {
                var closestTargetId = Entity.NO_ID
                var closestTargetPosition = target.initial
                var minDistanceSq = Double.MAX_VALUE

                for (unit in engine.getWithComponent(UnitComponent::class)) {
                    val (otherAffiliation, otherTransform, otherUnit) = unit.getEnabled(
                        AffiliationComponent::class, TransformComponent::class, UnitComponent::class
                    ) ?: continue

                    if (affiliation.player == otherAffiliation.player || otherUnit.type !in target.targetTypes) {
                        continue
                    }

                    val distanceSq = transform.position distanceToSq otherTransform.position
                    val isSuitableTarget = ((
                            target.lockType == LockType.LineOfSight
                                    && transform.hasLineOfSight(lineOfSight, otherTransform.position)
                            ) || (target.lockType == LockType.Always))
                            && target.minLockDistance.sq <= distanceSq && distanceSq <= target.maxLockDistance.sq

                    if (isSuitableTarget && distanceSq < minDistanceSq) {
                        closestTargetId = unit.id
                        closestTargetPosition = otherTransform.position
                        minDistanceSq = distanceSq
                    }
                }

                if (closestTargetId != Entity.NO_ID) {
                    target.id = closestTargetId
                    target.current = closestTargetPosition
                    target.timeWithoutTarget = 0

                    break
                }

                target.timeWithoutTarget += engine.actualTimePeriod

                if (target.mustReset && target.current != target.initial) {
                    target.current = target.initial
                }
            }

            if (target.current.x > bounds.x || target.current.y > bounds.y
                || target.current.x < 0.0 || target.current.y < 0.0
            ) {
                target.id = Entity.NO_ID
                target.current = returnTo
            }
        }
    }
}

/**
 * Checks if v is within circle segment defined by transform and line of sight parameters
 */
fun TransformComponent.hasLineOfSight(
    lineOfSight: LineOfSightComponent,
    v: Vector
): Boolean {
    val vectorToTarget = (v - this.position).rotated(-this.rotation)
    val distanceToTargetSq = vectorToTarget.lengthSq

    if (distanceToTargetSq > lineOfSight.distance.sq) {
        return false
    }
    if (lineOfSight.coneOfView == 2 * PI) {
        return true
    }

    val angle = vectorToTarget.angle()

    return angle <= lineOfSight.coneOfView / 2 && angle >= -lineOfSight.coneOfView / 2
}
