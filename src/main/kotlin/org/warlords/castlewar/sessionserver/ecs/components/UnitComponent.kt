package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

/**
 * I will come up with a better name later.
 */
class UnitComponent(
    val type: UnitType
) : Component()

enum class UnitType {
    Air,
    Ground,
    Naval,
    Building
}
