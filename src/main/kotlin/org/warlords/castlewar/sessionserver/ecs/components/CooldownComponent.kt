package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class CooldownComponent(val cooldown: Int, readyOnStart: Boolean = false) : Component() {

    var timeLeft: Int = if (readyOnStart) 0 else cooldown
}
