package org.warlords.castlewar.sessionserver.ecs.entities.attacks

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.utility.AIR_LAYER
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class AntiAircraftMissile(
    position: Vector, rotation: Double,
    targetId: Int, targetPosition: Vector,
    player: Player
) : Attack(
    TransformComponent(position, rotation),
    CollisionComponent(CircleMask(COLLISION_RADIUS), AIR_LAYER, destroyOnCollision = true),
    CollisionDamageComponent(DAMAGE),
    AffiliationComponent(player),
    LifetimeComponent(LIFETIME),
    TargetComponent(targetId, targetPosition),
    SpeedComponent(SPEED),
    RotateComponent(ROTATE_SPEED)
) {

    override val entityTypeName: String
        get() = "AntiAircraftMissile"

    override fun onCreated(engine: Engine) {
        get(CollisionComponent::class)!!.destroyOnCollision = true
    }

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val SPEED: Double = Parameters["Attack"]["AntiAircraftMissile"]["speed"] as Double
        private val ROTATE_SPEED: Double = Parameters["Attack"]["AntiAircraftMissile"]["rotate_speed"] as Double

        val SEEK_TIME: Int = Parameters["Attack"]["AntiAircraftMissile"]["seek_time"] as Int
        private val LIFETIME: Int = Parameters["Attack"]["AntiAircraftMissile"]["lifetime"] as Int

        val COLLISION_RADIUS: Double = Parameters["Attack"]["AntiAircraftMissile"]["collision_radius"] as Double

        val DAMAGE: Int = Parameters["Attack"]["AntiAircraftMissile"]["damage"] as Int

        val systems = Attack.systems + arrayOf(
            SeekerSystem::class,
            BulletSystem::class
        )
    }
}
