package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.AffiliationComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.components.UnitComponent
import org.warlords.castlewar.sessionserver.ecs.entities.misc.Explosion
import org.warlords.castlewar.sessionserver.utility.other

class DeathSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val health = entity.getEnabled(HealthComponent::class) ?: continue

            if (!health.alive) {
                engine.destroyLater(entity.id)

                entity.get(AffiliationComponent::class)?.let { affiliation ->
                    val otherPlayer = affiliation.player.other()
                    engine.get(EconomicsSystem::class)?.run {
                        minerals[otherPlayer] =
                            minerals.getOrDefault(otherPlayer, defaultValue = 0) + (0.02 * health.max).toInt()
                    }
                }

                if (entity hasEnabled UnitComponent::class) {
                    val transform = entity.getEnabled(TransformComponent::class) ?: continue
                    engine.createLater(Explosion(transform.position))
                }
            }
        }
    }
}