package org.warlords.castlewar.sessionserver.ecs.base

import org.warlords.castlewar.sessionserver.utility.*
import org.warlords.castlewar.proto.Castlewar
import org.warlords.castlewar.sessionserver.ecs.components.AffiliationComponent
import org.warlords.castlewar.sessionserver.ecs.components.CooldownComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.EntityTypeId
import kotlin.reflect.KClass
import kotlin.reflect.cast
import kotlin.reflect.safeCast

abstract class Entity(
    vararg components: Component,
    var enabled: Boolean = true
) {

    private val components = listOf(*components)
    private val cache = mutableMapOf<KClass<out Component>, Component>()

    internal fun bindComponents() = components.forEach { it.internalEntity = this }

    protected abstract val entityTypeName: String

    val id: Int = nextId

    /**
     * Tests is this entity has a component of a specified class.
     */
    infix fun <T : Component> hasEnabled(cls: KClass<T>): Boolean {
        if (cache.containsKey(cls) && cache[cls]!!.enabled) {
            return true
        }

        // cache component because it is likely to be requested immediately after
        val component = getEnabled(cls)
        return component != null
    }

    /**
     * Returns a component of class `cls` if this entity has such component enabled and `null` otherwise
     * If this entity has more than one component of class `cls` enabled, the returned component is the one added
     * earlier.
     */
    fun <T : Component> getEnabled(cls: KClass<T>): T? =
        cache[cls]?.let { cachedComponent ->
            if (cachedComponent.enabled) {
                cls.safeCast(cachedComponent)
            } else {
                getAllEnabled(cls).firstOrNull()
            }
        } ?: components.find { cls.isInstance(it) && it.enabled }?.let {
            cache[cls] = it
            cls.cast(it)
        }

    /**
     * See [Entity.getEnabled]
     */
    fun <T1 : Component, T2 : Component> getEnabled(cls1: KClass<T1>, cls2: KClass<T2>): Tuple2<T1, T2>? {
        val c1 = getEnabled(cls1) ?: return null
        val c2 = getEnabled(cls2) ?: return null

        return Tuple2(c1, c2)
    }

    /**
     * See [Entity.getEnabled]
     */
    fun <T1 : Component, T2 : Component, T3 : Component> getEnabled(
        cls1: KClass<T1>,
        cls2: KClass<T2>,
        cls3: KClass<T3>
    ): Tuple3<T1, T2, T3>? {
        val c1 = getEnabled(cls1) ?: return null
        val c2 = getEnabled(cls2) ?: return null
        val c3 = getEnabled(cls3) ?: return null

        return Tuple3(c1, c2, c3)
    }

    /**
     * See [Entity.getEnabled]
     */
    fun <T1 : Component, T2 : Component, T3 : Component, T4 : Component> getEnabled(
        cls1: KClass<T1>,
        cls2: KClass<T2>,
        cls3: KClass<T3>,
        cls4: KClass<T4>
    ): Tuple4<T1, T2, T3, T4>? {
        val c1 = getEnabled(cls1) ?: return null
        val c2 = getEnabled(cls2) ?: return null
        val c3 = getEnabled(cls3) ?: return null
        val c4 = getEnabled(cls4) ?: return null

        return Tuple4(c1, c2, c3, c4)
    }

    /**
     * See [Entity.getEnabled]
     */
    fun <T1 : Component, T2 : Component, T3 : Component, T4 : Component, T5 : Component> getEnabled(
        cls1: KClass<T1>,
        cls2: KClass<T2>,
        cls3: KClass<T3>,
        cls4: KClass<T4>,
        cls5: KClass<T5>
    ): Tuple5<T1, T2, T3, T4, T5>? {
        val c1 = getEnabled(cls1) ?: return null
        val c2 = getEnabled(cls2) ?: return null
        val c3 = getEnabled(cls3) ?: return null
        val c4 = getEnabled(cls4) ?: return null
        val c5 = getEnabled(cls5) ?: return null

        return Tuple5(c1, c2, c3, c4, c5)
    }

    /**
     * Returns a component of class `cls` if this entity has such component and `null` otherwise
     * If this entity has more than one component of class `cls`, the returned component is the one added
     * earlier.
     */
    fun <T : Component> get(cls: KClass<T>): T? =
        cache[cls]?.let {
            cls.safeCast(it)
        } ?: components.find { cls.isInstance(it) && it.enabled }?.let {
            cache[cls] = it
            cls.cast(it)
        }

    /**
     * Returns a list of all components of class `cls` in order they were provided to this entity.
     *
     * Returns enabled components only.
     */
    fun <T : Component> getAllEnabled(cls: KClass<T>): List<T> {
        return components
            .filter { cls.isInstance(it) && it.enabled }
            .map { cls.cast(it) }
    }

    /**
     * Returns a list of all components of class `cls` in order they were provided to this entity.
     */
    fun <T : Component> getAll(cls: KClass<T>): List<T> {
        return components
            .filter { cls.isInstance(it) }
            .map { cls.cast(it) }
    }

    open fun onCreated(engine: Engine) = Unit

    open fun onDestroyed(engine: Engine) = Unit

    /**
     * This entity will be registered for systems returned by this method when added via [Engine.createLater].
     */
    abstract fun getSystems(): Array<KClass<out System>>

    fun serialize(): Castlewar.Entity {
        return Castlewar.Entity.newBuilder().apply {
            val transform = get(TransformComponent::class)!!
            setTransform(
                Castlewar.Transform.newBuilder()
                    .setX(transform.position.x)
                    .setY(transform.position.y)
                    .setRotation(transform.rotation)
                    .build()
            )
            get(HealthComponent::class)?.let { health = it.current }
            get(CooldownComponent::class)?.let { cooldown = it.timeLeft }
            get(AffiliationComponent::class)?.let { player = it.player }
            typeId = EntityTypeId[entityTypeName]
        }.build()
    }

    override fun equals(other: Any?): Boolean = other != null && other is Entity && id == other.id

    override fun hashCode(): Int = id

    companion object {

        internal var nextId = 0
            get() {
                val id = field
                field += 1
                return id
            }
            set(_) = Unit

        const val NO_ID = Int.MIN_VALUE
    }
}