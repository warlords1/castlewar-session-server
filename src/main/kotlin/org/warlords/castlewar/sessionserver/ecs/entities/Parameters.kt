package org.warlords.castlewar.sessionserver.ecs.entities

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

object Parameters {

    private const val PARAMETERS_FILE = "/entity_parameters.json"
    private val parameters = ObjectMapper()
        .readValue<Map<String, Map<String, Map<String, Any>>>>(Parameters::class.java.getResource(PARAMETERS_FILE))
        .map { (key, value) -> key to MapWrapper(value) }
        .toMap()

    operator fun get(key: String): MapWrapper = parameters.getValue(key)

    class MapWrapper(private val map: Map<String, Map<String, Any>>) {

        operator fun get(key: String): Map<String, Any> = map.getValue(key)

        val keys
            get() = map.keys
    }
}