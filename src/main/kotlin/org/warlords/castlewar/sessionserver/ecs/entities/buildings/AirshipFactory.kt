package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.units.Tank
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.units.Airship
import org.warlords.castlewar.sessionserver.ecs.systems.UnitSpawnSystem
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class AirshipFactory(
    position: Vector,
    rallyPoint: Vector,
    player: Player
) : Building(
    TransformComponent(position),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    HealthComponent(MAX_HEALTH),
    TargetComponent(NO_ID, rallyPoint),
    UnitSpawnerComponent(Airship::class),
    CooldownComponent(SPAWN_COOLDOWN)
) {

    override val entityTypeName: String
        get() = "AirshipFactory"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        val COLLISION_RADIUS: Double = Parameters["Building"]["AirshipFactory"]["collision_radius"] as Double

        val MAX_HEALTH: Int = Parameters["Building"]["AirshipFactory"]["max_health"] as Int

        val SPAWN_COOLDOWN: Int = Parameters["Building"]["AirshipFactory"]["spawn_cooldown"] as Int

        private val systems = Building.systems + UnitSpawnSystem::class
    }
}
