package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class RotateComponent(val rotateSpeed: Double = INSTANT) : Component() {

    companion object {

        const val INSTANT = 0.0
    }
}
