package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.RotateComponent
import org.warlords.castlewar.sessionserver.ecs.components.TargetComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.math.abs

class SeekerSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (transform, rotate, target) = entity.getEnabled(
                TransformComponent::class, RotateComponent::class, TargetComponent::class
            ) ?: continue

            transform.rotateTowardsObject(target.current, rotate, engine.timeScale)
        }
    }
}

fun TransformComponent.rotateTowardsObject(
    dest: Vector,
    rotate: RotateComponent,
    timeScale: Double) {
    val toDest = dest - this.position

    val angle = toDest.angleRelativeTo(this.direction)

    if (rotate.rotateSpeed == RotateComponent.INSTANT) {
        this.rotation += angle
        return
    }

    val angularSpeed = rotate.rotateSpeed * timeScale

    val rotateAngle = when {
        abs(angle) <= angularSpeed -> angle
        angle < 0 -> -angularSpeed
        else -> angularSpeed
    }

    val newDirection = this.direction.copy()

    val angles = listOf(angularSpeed, -angularSpeed, rotateAngle)
        .map { toDest distanceTo newDirection.rotated(it) to it }
        .toMap()

    this.rotation += angles[angles.keys.minOrNull()!!]!!
}
