package org.warlords.castlewar.sessionserver.ecs.entities.units

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.GameObject
import org.warlords.castlewar.sessionserver.ecs.systems.DeathSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SerializationSystem

abstract class Unit(
    transform: TransformComponent,
    collision: CollisionComponent,
    affiliation: AffiliationComponent,
    target: TargetComponent,
    unit: UnitComponent,
    health: HealthComponent,
    vararg components: Component
) : GameObject(transform, collision, affiliation, target, unit, health, *components) {

    companion object {

        val systems = GameObject.systems + arrayOf(DeathSystem::class, SerializationSystem::class)
    }
}
