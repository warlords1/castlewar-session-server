package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.units.BomberPlane
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class BomberAirfield(
    position: Vector,
    rallyPoint: Vector,
    player: Player
) : Building(
    TransformComponent(position),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    HealthComponent(MAX_HEALTH),
    TargetComponent(NO_ID, rallyPoint),
    UnitSpawnerComponent(BomberPlane::class),
    CooldownComponent(SPAWN_COOLDOWN)
) {

    override val entityTypeName: String
        get() = "BomberAirfield"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        val COLLISION_RADIUS: Double = Parameters["Building"]["BomberAirfield"]["collision_radius"] as Double

        val MAX_HEALTH: Int = Parameters["Building"]["BomberAirfield"]["max_health"] as Int

        val SPAWN_COOLDOWN: Int = Parameters["Building"]["BomberAirfield"]["spawn_cooldown"] as Int

        private val systems = Building.systems + UnitSpawnSystem::class
    }
}