package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthRegenComponent

class HealthRegenSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (health, regen) = entity.getEnabled(HealthComponent::class, HealthRegenComponent::class) ?: continue
            val healing = regen.rate * engine.timeScale
            health.current += healing.toInt()
        }
    }
}