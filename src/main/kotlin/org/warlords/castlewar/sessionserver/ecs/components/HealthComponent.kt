package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class HealthComponent(
    val max: Int,
    current: Int = max
) :
    Component() {

    var current: Int = current
        set(value) {
            field = when {
                value > max -> max
                value < 0 -> 0
                else -> value
            }
        }

    val alive
        get() = current > 0
}
