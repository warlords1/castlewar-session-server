package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class HealthRegenComponent(val rate: Int) : Component()
