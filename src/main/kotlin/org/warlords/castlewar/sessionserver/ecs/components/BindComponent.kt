package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class BindComponent(val id: Int) : Component()
