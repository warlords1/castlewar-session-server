package org.warlords.castlewar.sessionserver.ecs.base

abstract class System(var enabled: Boolean = true) {

    internal lateinit var engine: Engine

    abstract fun update(elapsedTime: Int, entities: Collection<Entity>)
}
