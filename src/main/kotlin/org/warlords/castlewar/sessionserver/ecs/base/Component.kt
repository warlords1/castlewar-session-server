package org.warlords.castlewar.sessionserver.ecs.base

abstract class Component(var enabled: Boolean = true) {

    internal var internalEntity: Entity? = null

    val entity: Entity
        get() = internalEntity!!

    val hasEntity: Boolean
        get() = internalEntity != null
}
