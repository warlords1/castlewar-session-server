package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class LineOfSightComponent(val distance: Double, val coneOfView: Double): Component()
