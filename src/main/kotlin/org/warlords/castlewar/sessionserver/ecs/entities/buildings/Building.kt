package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.GameObject
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.systems.BuildingPlacementSystem
import org.warlords.castlewar.sessionserver.ecs.systems.DeathSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SerializationSystem

abstract class Building(
    transform: TransformComponent,
    collision: CollisionComponent,
    affiliation: AffiliationComponent,
    health: HealthComponent,
    vararg components: Component
) : GameObject(
    transform, collision, affiliation, health,
    UnitComponent(UnitType.Building),
    *components
) {

    data class BuildCost(val minerals: Int, val terrazine: Int)

    companion object {

        val buildCost = mutableMapOf(
            BomberAirfield::class to BuildCost(
                Parameters["Building"]["BomberAirfield"]["minerals_build_cost"] as Int,
                Parameters["Building"]["BomberAirfield"]["terrazine_build_cost"] as Int
            ),
            FighterAirfield::class to BuildCost(
                Parameters["Building"]["FighterAirfield"]["minerals_build_cost"] as Int,
                Parameters["Building"]["FighterAirfield"]["terrazine_build_cost"] as Int
            ),
            HQ::class to BuildCost(
                Parameters["Building"]["HQ"]["minerals_build_cost"] as Int,
                Parameters["Building"]["HQ"]["terrazine_build_cost"] as Int
            ),
            TankFactory::class to BuildCost(
                Parameters["Building"]["TankFactory"]["minerals_build_cost"] as Int,
                Parameters["Building"]["TankFactory"]["terrazine_build_cost"] as Int
            ),
            LightTankFactory::class to BuildCost(
                Parameters["Building"]["LightTankFactory"]["minerals_build_cost"] as Int,
                Parameters["Building"]["LightTankFactory"]["terrazine_build_cost"] as Int
            ),
            SAM::class to BuildCost(
                Parameters["Building"]["SAM"]["minerals_build_cost"] as Int,
                Parameters["Building"]["SAM"]["terrazine_build_cost"] as Int
            ),
            Artillery::class to BuildCost(
                Parameters["Building"]["Artillery"]["minerals_build_cost"] as Int,
                Parameters["Building"]["Artillery"]["terrazine_build_cost"] as Int
            ),
            AirshipFactory::class to BuildCost(
                Parameters["Building"]["AirshipFactory"]["minerals_build_cost"] as Int,
                Parameters["Building"]["AirshipFactory"]["terrazine_build_cost"] as Int
            )
        )

        val systems = GameObject.systems + arrayOf(
            DeathSystem::class,
            BuildingPlacementSystem::class,
            SerializationSystem::class
        )
    }
}
