package org.warlords.castlewar.sessionserver.ecs.entities.misc

import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.Shell
import org.warlords.castlewar.sessionserver.ecs.systems.AttackSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SeekerSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SerializationSystem
import org.warlords.castlewar.sessionserver.ecs.systems.TurretBindSystem
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class ArtilleryTurret(
    position: Vector, rotation: Double,
    parentId: Int,
    player: Player,
) : Entity(
    TransformComponent(position, rotation),
    BindComponent(parentId),
    AffiliationComponent(player),
    AttackerComponent(Shell::class, 0.0, SIGHT_DISTANCE * 0.9),
    CooldownComponent(ATTACK_COOLDOWN, readyOnStart = true),
    RotateComponent(ROTATE_SPEED),
    TargetComponent()
) {

    override val entityTypeName: String
        get() = "ArtilleryTurret"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val ROTATE_SPEED: Double = Parameters["Building"]["Artillery"]["rotate_speed"] as Double

        private val SIGHT_DISTANCE: Double = Parameters["Building"]["Artillery"]["sight_distance"] as Double

        private val ATTACK_COOLDOWN: Int = Parameters["Building"]["Artillery"]["attack_cooldown"] as Int

        val systems = arrayOf(
            TurretBindSystem::class,
            AttackSystem::class,
            SeekerSystem::class,
            SerializationSystem::class
        )
    }
}