package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class SpeedComponent(val speed: Double) : Component()
