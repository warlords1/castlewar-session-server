package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.WIDTH
import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.buildings.*
import org.warlords.castlewar.sessionserver.logger
import org.warlords.castlewar.sessionserver.utility.*
import kotlin.reflect.KClass

class BuildingPlacementSystem(
    private val w: Double,
    private val h: Double,
    private val cellSize: Double
) : System() {

    private var alreadyPlaced: Collection<Entity> = emptyList()
    private val toBePlaced = mutableSetOf<Building>()

    @Synchronized
    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        alreadyPlaced = entities.filter { it.hasEnabled(TransformComponent::class) }
        toBePlaced.forEach { engine.createLater(it) }
        toBePlaced.clear()
    }

    @Synchronized
    fun placeBuilding(cls: KClass<out Building>, at: Vector, player: Player): String {
        at.x -= cellSize / 2
        at.y -= cellSize / 2

        at.x = at.x.round(cellSize)
        at.y = at.y.round(cellSize)

        at.x += cellSize / 2
        at.y += cellSize / 2

        if (at.x <= 0 || at.y <= 0 || at.x >= w || at.y >= h) {
            return "position"
        }

        if (player == LEFT_PLAYER && at.x >= w / 3 || player == RIGHT_PLAYER && at.x <= 2 * w / 3) {
            return "position"
        }

        if (alreadyPlaced.any { (it.getEnabled(TransformComponent::class)!!.position - at).lengthSq < cellSize.sq }) {
            return "position"
        }

        val buildCost = Building.buildCost[cls] ?: return "server_cost"
        val economicsSystem = engine.get(EconomicsSystem::class)!!
        if (economicsSystem.minerals[player]!! < buildCost.minerals) {
            return "resources"
        }
        if (economicsSystem.terrazine[player]!! < buildCost.terrazine) {
            return "resources"
        }

        val entity = instantiateBuilding(cls, at, player)
        entity ?: return "server_building"

        toBePlaced += entity
        economicsSystem.run {
            minerals[player] = minerals[player]!! - buildCost.minerals
            terrazine[player] = terrazine[player]!! - buildCost.terrazine
        }

        logger.info("ok")

        return "ok"
    }

    @Synchronized
    fun getBuilding(x: Double, y: Double): Entity? {
        val at = Vector(x, y)
        at.x -= cellSize / 2
        at.y -= cellSize / 2

        at.x = at.x.round(cellSize)
        at.y = at.y.round(cellSize)

        at.x += cellSize / 2
        at.y += cellSize / 2

        return alreadyPlaced.find { building ->
            building.get(TransformComponent::class)!!.position == at
        }
    }
}

internal fun instantiateBuilding(cls: KClass<out Building>, at: Vector, player: Player) = when (cls) {
    HQ::class -> HQ(at, player)
    BomberAirfield::class -> BomberAirfield(at, Vector(WIDTH - at.x, at.y), player)
    FighterAirfield::class -> FighterAirfield(at, Vector(WIDTH - at.x, at.y), player)
    TankFactory::class -> TankFactory(at, Vector(WIDTH - at.x, at.y), player)
    LightTankFactory::class -> LightTankFactory(at, Vector(WIDTH - at.x, at.y), player)
    SAM::class -> SAM(at, player)
    Artillery::class -> Artillery(at, player)
    AirshipFactory::class -> AirshipFactory(at, Vector(WIDTH - at.x, at.y), player)
    else -> null
}
