package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class ResourceProducerComponent(val amount: Int, val type: ResourceType = ResourceType.Minerals) : Component()

enum class ResourceType {
    Minerals,
    Terrazine
}
