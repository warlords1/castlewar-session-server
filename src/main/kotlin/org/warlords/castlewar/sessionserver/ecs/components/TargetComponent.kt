package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.utility.Vector

class TargetComponent(
    var id: Int = Entity.NO_ID,
    var current: Vector = Vector(0.0, 0.0),

    val targetTypes: Array<UnitType> = UnitType.values(),
    val distanceBuffer: Double = 0.0,
    val lockType: LockType = LockType.LineOfSight,

    val minLockDistance: Double = 0.0,
    val maxLockDistance: Double = Double.MAX_VALUE,

    private val resetTime: Int = 0
) : Component() {

    val initial = current
    var timeWithoutTarget = 0

    val mustReset: Boolean
        get() = timeWithoutTarget >= resetTime
}

enum class LockType {
    Always,
    LineOfSight
}
