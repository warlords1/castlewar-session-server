package org.warlords.castlewar.sessionserver.ecs.base

import kotlin.reflect.KClass
import kotlin.reflect.cast
import kotlin.reflect.safeCast

class Engine(updatePeriod: Int = DEFAULT_UPDATE_PERIOD, vararg systems: System) {

    private val scheduler = SchedulerService()

    private val systems: MutableList<System> = mutableListOf(*systems)
    private val entities = mutableMapOf<Int, Entity>()
    private val systemEntities = mutableMapOf<KClass<out System>, MutableSet<Entity>>()

    private val newEntities = mutableSetOf<Pair<Entity, Array<out KClass<out System>>>>()
    private val deleteEntities = mutableSetOf<Int>()

    private var lastUpdateTime: Int = 0

    /**
     * Expected time period for [Engine.update] calls.
     */
    val timeUnit: Int = if (updatePeriod > 0) updatePeriod else DEFAULT_UPDATE_PERIOD

    /**
     * Time provided to the last [Engine.update] call.
     */
    var currentTime: Int = 0
        private set

    val actualTimePeriod: Int
        get() = currentTime - lastUpdateTime

    /**
     * Actual update time divided by expected update time.
     */
    val timeScale: Double
        get() = actualTimePeriod.toDouble() / timeUnit

    private var updateCycles = 0
    val averageTimeScale = (currentTime + 1) / (updateCycles + 1)

    private var creatingEntities: Boolean = false
    private var destroyingEntities: Boolean = false

    init {
        systems.forEach { it.engine = this }
    }

    /**
     * Does the following steps:
     *
     * * Calls [System.update] for every enabled system, passing `time` and a collection of entities registered for
     * the system;
     *
     * * Deletes and disables all entities passed to [Engine.destroyLater] during the update step;
     *
     * * Adds all entities passed to [Engine.createLater] during the update step and registers them for requested systems. See [Entity.getSystems].
     */
    fun update(time: Int) {
        currentTime = time

        for (system in systems) {
            if (system.enabled) {
                system.update(currentTime, systemEntities[system::class] ?: emptyList())
            }
        }

        scheduler.update(currentTime)

        destroyingEntities = true
        deleteEntities.forEach { id -> entities[id]?.onDestroyed(this) }
        entities -= deleteEntities
        for ((_, entities) in systemEntities) {
            entities.removeIf { entity -> entity.id in deleteEntities }
        }
        deleteEntities.clear()
        destroyingEntities = false

        creatingEntities = true
        for ((entity, extraSystems) in newEntities) {
            addEntityInternal(entity, extraSystems)
            entity.onCreated(this)
        }
        newEntities.clear()
        creatingEntities = false

        lastUpdateTime = time
        updateCycles += 1
    }

    /**
     * Schedule an `entity` to be added and registered for requested `systems` after
     * current update ends.
     *
     * Entity will be registered for every system in `systems`, as well as for every system returned by
     * [Entity.getSystems].
     */
    fun createLater(entity: Entity, vararg extraSystems: KClass<out System>): Int {
        if (creatingEntities) {
            throw IllegalStateException("creating entities right now")
        }
        newEntities += entity to extraSystems
        return entity.id
    }

    fun createNow(entity: Entity, vararg extraSystems: KClass<out System>): Int {
        addEntityInternal(entity, extraSystems)
        return entity.id
    }

    private fun addEntityInternal(entity: Entity, extraSystems: Array<out KClass<out System>>) {
        entity.bindComponents()

        entities[entity.id] = entity

        for (system in entity.getSystems()) {
            registerEntitySystem(entity, system)
        }
        for (system in extraSystems) {
            registerEntitySystem(entity, system)
        }
    }

    private fun <T : System> registerEntitySystem(entity: Entity, system: KClass<T>) {
        systemEntities[system]?.let {
            it += entity
        } ?: run {
            systemEntities[system] = mutableSetOf(entity)
        }
    }

    /**
     * Schedule an entity with id `entityId` to be deleted after current update ends.
     */
    fun destroyLater(entityId: Int) {
        if (destroyingEntities) {
            throw IllegalStateException("destroying entities right now")
        }
        deleteEntities += entityId
    }

    fun runAfter(delay: Int, action: () -> Unit) = scheduler.executeAfter(currentTime, delay, action)

    fun runLater(action: () -> Unit) = scheduler.executeAfter(currentTime, 0, action)

    /**
     * Get the first added system of class `cls`.
     */
    fun <T : System> get(cls: KClass<T>): T? {
        return cls.safeCast(systems.find { cls.isInstance(it) })
    }

    /**
     * Get all entities of class `cls`.
     */
    fun <T : Entity> getEntities(cls: KClass<T>): Collection<T> = entities
        .filter { cls.isInstance(it) }
        .map { cls.cast(it) }

    /**
     * Returns an entity with given `entityId` or `null` if there is no such entity.
     */
    fun getById(entityId: Int): Entity? = entities[entityId]

    /**
     * Tests if an entity with given `entityId` exists.
     */
    fun exists(entityId: Int) = entityId in entities.keys

    /**
     * Get all entities with component of class `cls`.
     */
    fun <T : Component> getWithComponent(cls: KClass<T>, enabledOnly: Boolean = true): Collection<Entity> =
        entities.filter { (_, it) ->
            val c = it.getEnabled(cls)
            (c != null && c.enabled && enabledOnly) || (!enabledOnly && c != null)
        }.values

    companion object {

        const val DEFAULT_UPDATE_PERIOD: Int = 1000
    }
}

private class SchedulerService {

    private val scheduledActions = sortedMapOf<Int, MutableList<() -> Unit>>()

    fun update(elapsedTime: Int) {
        val done = mutableListOf<Int>()
        for ((executionTime, actions) in scheduledActions) {
            if (executionTime > elapsedTime) {
                break
            }

            actions.forEach { it() }
            done += executionTime
        }
        done.forEach { scheduledActions -= it }
    }

    /**
     * Schedule an action to be performed after a delay.
     */
    fun executeAfter(now: Int, delay: Int, action: () -> Unit) {
        val executionTime = now + delay
        if (scheduledActions.containsKey(executionTime)) {
            scheduledActions[executionTime]!! += action
        } else {
            scheduledActions[executionTime] = mutableListOf(action)
        }
    }
}
