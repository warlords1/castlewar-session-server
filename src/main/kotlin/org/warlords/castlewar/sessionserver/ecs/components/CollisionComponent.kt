package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.utility.CollisionMask

class CollisionComponent(
    val mask: CollisionMask,
    val layer: Int = 0,
    var destroyOnCollision: Boolean = false,
    var disableOnCollision: Boolean = false
) : Component()
