package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.AffiliationComponent
import org.warlords.castlewar.sessionserver.ecs.components.CooldownComponent
import org.warlords.castlewar.sessionserver.ecs.components.ResourceProducerComponent
import org.warlords.castlewar.sessionserver.ecs.components.ResourceType
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER

class EconomicsSystem(
    leftPlayerMinerals: Int = 0,
    leftPlayerTerrazine: Int = 0,
    rightPlayerMinerals: Int = 0,
    rightPlayerTerrazine: Int = 0
) : System() {

    val minerals = mutableMapOf(LEFT_PLAYER to leftPlayerMinerals, RIGHT_PLAYER to rightPlayerMinerals)
    val terrazine = mutableMapOf(LEFT_PLAYER to leftPlayerTerrazine, RIGHT_PLAYER to rightPlayerTerrazine)

    @Synchronized
    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (affiliation, cooldown, producer) = entity.getEnabled(
                AffiliationComponent::class, CooldownComponent::class, ResourceProducerComponent::class
            ) ?: continue

            if (cooldown.timeLeft > 0) {
                cooldown.timeLeft -= engine.actualTimePeriod
                continue
            }

            val resource = when (producer.type) {
                ResourceType.Minerals -> minerals
                ResourceType.Terrazine -> terrazine
            }
            resource[affiliation.player] = (resource[affiliation.player] ?: 0) + producer.amount
            cooldown.timeLeft = cooldown.cooldown
        }
    }

    @Synchronized
    fun run(action: EconomicsSystem.() -> Unit) = action(this)
}