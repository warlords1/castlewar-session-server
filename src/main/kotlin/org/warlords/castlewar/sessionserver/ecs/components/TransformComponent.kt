package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.utility.Vector
import org.warlords.castlewar.sessionserver.ecs.base.Component

class TransformComponent(
    x: Double, y: Double,
    var rotation: Double
) : Component() {

    val position = Vector(x, y)

    constructor(position: Vector) : this(position.x, position.y, 0.0)

    constructor(position: Vector, rotation: Double) : this(position.x, position.y, rotation)

    constructor(x: Double, y: Double) : this(x, y, 0.0)

    val direction: Vector
        get() = Vector.e.rotated(rotation).normalized()
}
