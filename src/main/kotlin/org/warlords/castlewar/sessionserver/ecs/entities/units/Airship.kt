package org.warlords.castlewar.sessionserver.ecs.entities.units

import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.Bomb
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.utility.AIR_LAYER
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class Airship(
    position: Vector, rotation: Double = 0.0,
    targetId: Int, targetPosition: Vector,
    player: Player
) :
    Unit(
        TransformComponent(position, rotation),
        CollisionComponent(CircleMask(COLLISION_RADIUS), AIR_LAYER),
        AffiliationComponent(player),
        TargetComponent(
            targetId, targetPosition,
            TARGET_TYPES, SPEED * 1.5,
            resetTime = TARGET_RESET_TIME
        ),
        UnitComponent(UnitType.Air),
        HealthComponent(MAX_HEALTH),
        HealthRegenComponent(HEALTH_REGEN),
        AttackerComponent(Bomb::class, 0.0, BOMB_DROP_DISTANCE),
        LineOfSightComponent(SIGHT_DISTANCE, CONE_OF_VIEW),
        CooldownComponent(ATTACK_COOLDOWN, readyOnStart = true),
        SpeedComponent(SPEED),
        RotateComponent(ROTATE_SPEED)
    ) {

    override val entityTypeName: String
        get() = "Airship"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val SPEED: Double = Parameters["Unit"]["Airship"]["speed"] as Double
        private val ROTATE_SPEED: Double = Parameters["Unit"]["Airship"]["rotate_speed"] as Double

        val MAX_HEALTH: Int = Parameters["Unit"]["Airship"]["max_health"] as Int

        private val HEALTH_REGEN: Int = Parameters["Unit"]["Airship"]["regen_rate"] as Int

        val COLLISION_RADIUS: Double = Parameters["Unit"]["Airship"]["collision_radius"] as Double

        private val SIGHT_DISTANCE: Double = Parameters["Unit"]["Airship"]["sight_distance"] as Double
        private val CONE_OF_VIEW: Double = Parameters["Unit"]["Airship"]["cone_of_view"] as Double

        private val ATTACK_COOLDOWN: Int = Parameters["Unit"]["Airship"]["attack_cooldown"] as Int

        private val TARGET_RESET_TIME: Int = Parameters["Unit"]["Airship"]["target_reset_time"] as Int

        private val BOMB_DROP_DISTANCE: Double = Parameters["Unit"]["Airship"]["bomb_drop_distance"] as Double

        private val TARGET_TYPES = (Parameters["Unit"]["Airship"]["target_types"] as List<*>)
            .map { UnitType.valueOf(it as String) }
            .toTypedArray()

        val systems = Unit.systems + arrayOf(
            SeekerSystem::class,
            BulletSystem::class,
            AttackSystem::class,
            LineOfSightTargetSystem::class,
            HealthRegenSystem::class
        )
    }
}