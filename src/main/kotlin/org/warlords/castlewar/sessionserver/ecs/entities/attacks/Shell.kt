package org.warlords.castlewar.sessionserver.ecs.entities.attacks

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.systems.BulletSystem
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.LineSegmentMask
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class Shell(
    position: Vector,
    rotation: Double,
    player: Player
) : Attack(
    TransformComponent(position, rotation),
    CollisionComponent(LineSegmentMask(Vector(LENGTH, 0.0)), GROUND_LAYER),
    CollisionDamageComponent(DAMAGE),
    AffiliationComponent(player),
    LifetimeComponent(LIFETIME),
    SpeedComponent(SPEED)
) {

    override val entityTypeName: String
        get() = "Shell"

    override fun onCreated(engine: Engine) {
        get(CollisionComponent::class)!!.destroyOnCollision = true
    }

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val SPEED: Double = Parameters["Attack"]["Shell"]["speed"] as Double

        private val LIFETIME: Int = Parameters["Attack"]["Shell"]["lifetime"] as Int
        private val DAMAGE: Int = Parameters["Attack"]["Shell"]["damage"] as Int

        val LENGTH: Double = Parameters["Attack"]["Shell"]["length"] as Double

        val systems = Attack.systems + BulletSystem::class

    }
}