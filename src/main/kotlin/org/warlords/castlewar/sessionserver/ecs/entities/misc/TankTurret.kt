package org.warlords.castlewar.sessionserver.ecs.entities.misc

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.Shell
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.AttackSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SeekerSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SerializationSystem
import org.warlords.castlewar.sessionserver.ecs.systems.TurretBindSystem
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class TankTurret(
    position: Vector, rotation: Double,
    parentId: Int,
    player: Player,
) : Entity(
    TransformComponent(position, rotation),
    BindComponent(parentId),
    AffiliationComponent(player),
    AttackerComponent(Shell::class, 0.0, SIGHT_DISTANCE * 0.75),
    CooldownComponent(ATTACK_COOLDOWN, readyOnStart = true),
    RotateComponent(ROTATE_SPEED),
    TargetComponent()
) {

    override val entityTypeName: String
        get() = "TankTurret"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val ROTATE_SPEED: Double = Parameters["Unit"]["Tank"]["rotate_speed"] as Double

        private val SIGHT_DISTANCE: Double = Parameters["Unit"]["Tank"]["sight_distance"] as Double

        private val ATTACK_COOLDOWN: Int = Parameters["Unit"]["Tank"]["attack_cooldown"] as Int

        val systems = arrayOf(
            TurretBindSystem::class,
            AttackSystem::class,
            SeekerSystem::class,
            SerializationSystem::class
        )
    }
}