package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.proto.Castlewar
import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER

class SerializationSystem(private val sendStateTo: (Castlewar.State) -> Unit) : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        val builder = Castlewar.State.newBuilder()
        for (entity in entities) {
            builder.addEntities(entity.serialize())
        }
        engine.get(EconomicsSystem::class)!!.run {
            builder.leftPlayer = Castlewar.Player.newBuilder()
                .setMinerals(minerals[LEFT_PLAYER]!!)
                .setTerrazine(terrazine[LEFT_PLAYER]!!)
                .setSupply(0)
                .build()
            builder.rightPlayer = Castlewar.Player.newBuilder()
                .setMinerals(minerals[RIGHT_PLAYER]!!)
                .setTerrazine(terrazine[RIGHT_PLAYER]!!)
                .setSupply(0)
                .build()
        }
        sendStateTo(builder.build())
    }
}
