package org.warlords.castlewar.sessionserver.ecs.entities.attacks

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.entities.GameObject
import org.warlords.castlewar.sessionserver.ecs.systems.LifetimeSystem
import org.warlords.castlewar.sessionserver.ecs.systems.SerializationSystem
import org.warlords.castlewar.sessionserver.ecs.components.*

abstract class Attack(
    transform: TransformComponent,
    collision: CollisionComponent,
    collisionDamage: CollisionDamageComponent,
    affiliation: AffiliationComponent,
    lifetime: LifetimeComponent,
    vararg components: Component
) : GameObject(
    transform, collision, affiliation, lifetime, collisionDamage,
    *components
) {

    override fun onCreated(engine: Engine) {
        get(CollisionComponent::class)!!.disableOnCollision = true
    }

    companion object {

        val systems = GameObject.systems + arrayOf(LifetimeSystem::class, SerializationSystem::class)
    }
}