package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.WIDTH
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.misc.SAMTurret
import org.warlords.castlewar.sessionserver.ecs.entities.units.Tank
import org.warlords.castlewar.sessionserver.ecs.systems.LineOfSightTargetSystem
import org.warlords.castlewar.sessionserver.utility.*
import kotlin.math.PI
import kotlin.reflect.KClass

class SAM(
    position: Vector,
    player: Player
) : Building(
    TransformComponent(position, if (LEFT_PLAYER == player) 0.0 else PI),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    HealthComponent(MAX_HEALTH),
    TargetComponent(
        current = Vector(WIDTH - position.x, position.y),
        targetTypes = TARGET_TYPES,
        resetTime = TARGET_RESET_TIME
    ),
    LineOfSightComponent(SIGHT_DISTANCE, CONE_OF_VIEW),
) {

    override val entityTypeName: String
        get() = "SAM"

    override fun getSystems(): Array<KClass<out System>> = systems

    override fun onCreated(engine: Engine) {
        val tf = get(TransformComponent::class)!!
        engine.runLater {
            engine.createNow(
                SAMTurret(
                    tf.position, tf.rotation, id, get(AffiliationComponent::class)!!.player
                )
            )
        }
    }

    companion object {

        val COLLISION_RADIUS: Double = Parameters["Building"]["SAM"]["collision_radius"] as Double

        val MAX_HEALTH: Int = Parameters["Building"]["SAM"]["max_health"] as Int

        private val SIGHT_DISTANCE: Double = Parameters["Building"]["SAM"]["sight_distance"] as Double
        private const val CONE_OF_VIEW: Double = 2 * PI

        private val TARGET_RESET_TIME: Int = Parameters["Building"]["SAM"]["target_reset_time"] as Int

        private val TARGET_TYPES = (Parameters["Building"]["SAM"]["target_types"] as List<*>)
            .map { UnitType.valueOf(it as String) }
            .toTypedArray()

        private val systems = Building.systems + LineOfSightTargetSystem::class
    }
}