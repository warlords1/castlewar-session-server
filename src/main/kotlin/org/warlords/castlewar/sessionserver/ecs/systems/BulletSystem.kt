package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.SpeedComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent

class BulletSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (transform, speed) = entity.getEnabled(TransformComponent::class, SpeedComponent::class) ?: continue

            val delta = transform.direction * speed.speed * engine.timeScale
            transform.position += delta
        }
    }
}