package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.units.LightTank
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.UnitSpawnSystem
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class LightTankFactory(
    position: Vector,
    rallyPoint: Vector,
    player: Player
) : Building(
    TransformComponent(position),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    HealthComponent(MAX_HEALTH),
    TargetComponent(NO_ID, rallyPoint),
    UnitSpawnerComponent(LightTank::class),
    CooldownComponent(SPAWN_COOLDOWN)
) {

    override val entityTypeName: String
        get() = "LightTankFactory"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        val COLLISION_RADIUS: Double = Parameters["Building"]["LightTankFactory"]["collision_radius"] as Double

        val MAX_HEALTH: Int = Parameters["Building"]["LightTankFactory"]["max_health"] as Int

        val SPAWN_COOLDOWN: Int = Parameters["Building"]["LightTankFactory"]["spawn_cooldown"] as Int

        private val systems = Building.systems + UnitSpawnSystem::class
    }
}
