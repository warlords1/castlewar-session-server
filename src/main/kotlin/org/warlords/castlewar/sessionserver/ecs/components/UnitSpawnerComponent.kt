package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.entities.units.Unit
import kotlin.reflect.KClass

class UnitSpawnerComponent(val unitClass: KClass<out Unit>) : Component()
