package org.warlords.castlewar.sessionserver.ecs.entities.buildings

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class HQ(
    position: Vector,
    player: Player
) : Building(
    TransformComponent(position),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    HealthComponent(MAX_HEALTH),
    ResourceProducerComponent(RESOURCE_AMOUNT),
    CooldownComponent(RESOURCE_COOLDOWN, readyOnStart = true)
) {

    override val entityTypeName: String
        get() = "HQ"

    override fun onDestroyed(engine: Engine) {
        engine.get(UnitSpawnSystem::class)?.let {
            it.enabled = false
        }
        engine.get(GroundMovementSystem::class)?.let {
            it.enabled = false
        }
        engine.get(EconomicsSystem::class)?.let {
            it.enabled = false
        }
        engine.get(AttackSystem::class)?.let {
            it.enabled = false
        }
    }

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        val COLLISION_RADIUS: Double = Parameters["Building"]["HQ"]["collision_radius"] as Double

        val MAX_HEALTH: Int = Parameters["Building"]["HQ"]["max_health"] as Int

        val RESOURCE_COOLDOWN: Int = Parameters["Building"]["HQ"]["resource_cooldown"] as Int
        val RESOURCE_AMOUNT: Int = Parameters["Building"]["HQ"]["resource_amount"] as Int

        private val systems = Building.systems + EconomicsSystem::class
    }
}