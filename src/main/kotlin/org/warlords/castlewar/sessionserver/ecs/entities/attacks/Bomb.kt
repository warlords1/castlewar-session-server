package org.warlords.castlewar.sessionserver.ecs.entities.attacks

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.LifetimeComponent.Companion.DIE_INSTANTLY
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.misc.Explosion
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.BulletSystem
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class Bomb(
    position: Vector, rotation: Double,
    speed: Double,
    player: Player
) : Attack(
    TransformComponent(position, rotation),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    CollisionDamageComponent(DAMAGE),
    AffiliationComponent(player),
    LifetimeComponent(DIE_INSTANTLY),
    SpeedComponent(speed)
) {

    override val entityTypeName: String
        get() = "Bomb"

    override fun onCreated(engine: Engine) {
        get(CollisionComponent::class)!!.enabled = false
        get(LifetimeComponent::class)!!.enabled = false
    }

    override fun onDestroyed(engine: Engine) {
        engine.createLater(Explosion(get(TransformComponent::class)!!.position))
    }

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val COLLISION_RADIUS: Double = Parameters["Attack"]["Bomb"]["collision_radius"] as Double
        private val DAMAGE: Int = Parameters["Attack"]["Bomb"]["damage"] as Int

        private val systems = Attack.systems + BulletSystem::class
    }
}