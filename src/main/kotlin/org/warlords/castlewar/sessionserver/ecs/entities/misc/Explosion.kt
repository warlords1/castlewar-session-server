package org.warlords.castlewar.sessionserver.ecs.entities.misc

import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.LifetimeComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class Explosion(
    position: Vector
) : Entity(
    TransformComponent(position),
    LifetimeComponent(LIFETIME)
) {
    override val entityTypeName: String
        get() = "Explosion"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        val LIFETIME: Int = Parameters["Misc"]["Explosion"]["lifetime"] as Int

        private val systems = arrayOf(
            LifetimeSystem::class,
            SerializationSystem::class
        )
    }
}
