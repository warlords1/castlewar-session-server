package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.Vector
import org.warlords.castlewar.sessionserver.utility.sq
import kotlin.reflect.safeCast

class GroundMovementSystem(private val w: Double, private val h: Double) : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        outer@ for (entity in entities) {
            val (transform, speed, rotate, collision, target) = entity.getEnabled(
                TransformComponent::class, SpeedComponent::class, RotateComponent::class,
                CollisionComponent::class, TargetComponent::class
            ) ?: continue

            val distanceToTargetSq = transform.position distanceToSq target.current
            val mask = CircleMask::class.safeCast(collision.mask) ?: continue
            val initialPosition = transform.position.copy()
            val actualSpeed = speed.speed * engine.timeScale
            val actualMaxRotation = rotate.rotateSpeed * engine.timeScale

            val colliders = engine.getColliders(transform.position, mask.radius * 10, entity.id)
            val inCollision = colliders.any { entity collisionWith it != null }

            if (inCollision) {
                transform.position += transform.direction * actualSpeed
                continue
            }

            val attacker = entity.getEnabled(AttackerComponent::class) ?: continue
            if (engine.exists(target.id)
                && attacker.minRange.sq <= distanceToTargetSq && distanceToTargetSq <= attacker.maxRange.sq
            ) {
                if (attacker.stopInRange) {
                    continue
                }
                transform.rotation += actualMaxRotation * 0.3 *
                        if (entity.id % 2L == 0L) -1 else 1
                transform.position += transform.direction * actualSpeed
                continue
            }

            if (colliders.isEmpty()) {
                transform.rotateTowardsObject(target.current, rotate, engine.timeScale)
                transform.position += transform.direction * actualSpeed
                continue
            }

            val angleOptions = arrayOf(
                actualMaxRotation,
                actualMaxRotation / 2,
                0.0,
                -actualMaxRotation / 2,
                -actualMaxRotation
            )
            val moveOptions = angleOptions
                .map { it to transform.direction.rotated(it) * actualSpeed }
                .sortedBy { (_, delta) -> transform.position + delta distanceToSq target.current }
                .filter { (_, delta) ->
                    val pos = (transform.position + delta)
                    pos.x in 0.0..w && pos.y in 0.0..h
                }

            for ((angle, moveOption) in moveOptions) {
                // test all move options and see which allows to evade collisions
                transform.position += moveOption
                val clear = colliders.all { it collisionWith entity == null }
                if (clear) {
                    transform.rotation += angle
                    continue@outer
                }
                transform.position.setCoordinates(initialPosition)
            }

            // if there's no way to evade collisions, start turning in place
            transform.rotation += actualMaxRotation *
                    if (entity.id % 2L == 0L) -1 else 1 // I'm not very proud of this but hey, as long as it works
        }
    }
}

fun Engine.getColliders(p: Vector, maxDistance: Double, exceptId: Int): Collection<Entity> {
    val colliders = getWithComponent(CollisionComponent::class, enabledOnly = true)
    return colliders.filter {
        it.id != exceptId
                && it hasEnabled TransformComponent::class
                && it hasEnabled UnitComponent::class
                && (it.getEnabled(UnitComponent::class)!!.type == UnitType.Ground
                || it.getEnabled(UnitComponent::class)!!.type == UnitType.Building)
                && it.getEnabled(TransformComponent::class)!!.position distanceToSq p <= maxDistance.sq
    }
}
