package org.warlords.castlewar.sessionserver.ecs.entities.units

import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.Bomb
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.*
import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.utility.AIR_LAYER
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class BomberPlane(
    position: Vector, rotation: Double = 0.0,
    targetId: Int, targetPosition: Vector,
    player: Player
) :
    Unit(
        TransformComponent(position, rotation),
        CollisionComponent(CircleMask(COLLISION_RADIUS), AIR_LAYER),
        AffiliationComponent(player),
        TargetComponent(
            targetId, targetPosition,
            TARGET_TYPES, SPEED * 1.5,
            lockType = LockType.LineOfSight,
            minLockDistance = BOMB_DROP_DISTANCE * 0.95,
            maxLockDistance = SIGHT_DISTANCE,
            resetTime = TARGET_RESET_TIME
        ),
        UnitComponent(UnitType.Air),
        HealthComponent(MAX_HEALTH),
        AttackerComponent(Bomb::class, BOMB_DROP_DISTANCE * 0.5, BOMB_DROP_DISTANCE),
        LineOfSightComponent(SIGHT_DISTANCE, CONE_OF_VIEW),
        CooldownComponent(ATTACK_COOLDOWN, readyOnStart = true),
        SpeedComponent(SPEED),
        RotateComponent(ROTATE_SPEED)
    ) {

    override val entityTypeName: String
        get() = "BomberPlane"

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val SPEED: Double = Parameters["Unit"]["BomberPlane"]["speed"] as Double
        private val ROTATE_SPEED: Double = Parameters["Unit"]["BomberPlane"]["rotate_speed"] as Double

        val MAX_HEALTH: Int = Parameters["Unit"]["BomberPlane"]["max_health"] as Int

        val COLLISION_RADIUS: Double = Parameters["Unit"]["BomberPlane"]["collision_radius"] as Double

        private val SIGHT_DISTANCE: Double = Parameters["Unit"]["BomberPlane"]["sight_distance"] as Double
        private val CONE_OF_VIEW: Double = Parameters["Unit"]["BomberPlane"]["cone_of_view"] as Double

        private val ATTACK_COOLDOWN: Int = Parameters["Unit"]["BomberPlane"]["attack_cooldown"] as Int

        private val TARGET_RESET_TIME: Int = Parameters["Unit"]["BomberPlane"]["target_reset_time"] as Int

        private val BOMB_DROP_DISTANCE: Double = Parameters["Unit"]["BomberPlane"]["bomb_drop_distance"] as Double

        private val TARGET_TYPES = (Parameters["Unit"]["BomberPlane"]["target_types"] as List<*>)
            .map { UnitType.valueOf(it as String) }
            .toTypedArray()

        val systems = Unit.systems + arrayOf(
            SeekerSystem::class,
            BulletSystem::class,
            AttackSystem::class,
            LineOfSightTargetSystem::class,
        )
    }
}