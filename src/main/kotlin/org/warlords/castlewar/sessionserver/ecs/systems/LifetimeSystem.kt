package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.LifetimeComponent

class LifetimeSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val lifetime = entity.getEnabled(LifetimeComponent::class) ?: continue

            lifetime.timeLeft -= engine.actualTimePeriod
            if (!lifetime.anyTimeLeft) {
                engine.destroyLater(entity.id)
            }
        }
    }
}