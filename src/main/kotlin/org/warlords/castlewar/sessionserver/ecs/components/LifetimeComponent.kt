package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component

class LifetimeComponent(var timeLeft: Int) : Component() {

    val anyTimeLeft
        get() = timeLeft > 0

    companion object {

        const val DIE_INSTANTLY = 0
    }
}
