package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.units.*
import org.warlords.castlewar.sessionserver.ecs.entities.units.Unit
import kotlin.reflect.KClass

class UnitSpawnSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (transform, cooldown, target, affiliation, spawner) = entity.getEnabled(
                TransformComponent::class, CooldownComponent::class, TargetComponent::class,
                AffiliationComponent::class, UnitSpawnerComponent::class
            ) ?: continue

            if (cooldown.timeLeft > 0) {
                cooldown.timeLeft -= engine.actualTimePeriod
                continue
            }

            val rotation = (target.current - transform.position).angle()

            spawn(
                spawner.unitClass,
                transform, rotation,
                target,
                affiliation,
                engine
            )

            cooldown.timeLeft = cooldown.cooldown
        }
    }
}

fun spawn(
    cls: KClass<out Unit>,
    transform: TransformComponent,
    rotation: Double,
    target: TargetComponent,
    affiliation: AffiliationComponent,
    engine: Engine
) {
    when (cls) {
        BomberPlane::class -> engine.createLater(
            BomberPlane(
                transform.position, rotation = rotation,
                targetId = Entity.NO_ID,
                targetPosition = target.current,
                player = affiliation.player
            )
        )
        FighterPlane::class -> engine.createLater(
            FighterPlane(
                transform.position, rotation = rotation,
                targetId = Entity.NO_ID,
                targetPosition = target.current,
                player = affiliation.player
            )
        )
        Tank::class -> engine.createLater(
            Tank(
                transform.position, rotation = rotation,
                targetId = Entity.NO_ID,
                targetPosition = target.current,
                player = affiliation.player
            )
        )
        LightTank::class -> engine.createLater(
            LightTank(
                transform.position, rotation = rotation,
                targetId = Entity.NO_ID,
                targetPosition = target.current,
                player = affiliation.player
            )
        )
        Airship::class -> engine.createLater(
            Airship(
                transform.position, rotation = rotation,
                targetId = Entity.NO_ID,
                targetPosition = target.current,
                player = affiliation.player
            )
        )
    }
}
