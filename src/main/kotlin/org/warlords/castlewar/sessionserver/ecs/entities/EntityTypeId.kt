package org.warlords.castlewar.sessionserver.ecs.entities

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

object EntityTypeId {

    private const val TYPE_ID_FILE = "/type_id.json"
    private val typeIds = ObjectMapper()
        .readValue<Map<String, Int>>(Parameters::class.java.getResource(TYPE_ID_FILE))

    operator fun get(key: String): Int = typeIds.getValue(key)
}