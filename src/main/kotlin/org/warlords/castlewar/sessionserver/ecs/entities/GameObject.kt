package org.warlords.castlewar.sessionserver.ecs.entities

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.CollisionSystem
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.CollisionMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

abstract class GameObject(
    t: TransformComponent,
    c: CollisionComponent,
    a: AffiliationComponent,
    vararg components: Component
) : Entity(t, c, a, *components) {

    companion object {

        val systems = arrayOf<KClass<out System>>(CollisionSystem::class)
    }
}