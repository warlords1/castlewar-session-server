package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.misc.Explosion
import org.warlords.castlewar.sessionserver.utility.Vector
import org.warlords.castlewar.sessionserver.utility.at
import org.warlords.castlewar.sessionserver.utility.intersectionWith

class CollisionSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        val e = entities.toList()
        for (i in e.indices) {
            val entity = e[i]
            val affiliation = entity.getEnabled(AffiliationComponent::class) ?: continue

            for (j in (i + 1) until e.size) {
                val other = e[j]

                val otherAffiliation = other.getEnabled(AffiliationComponent::class) ?: continue

                if (affiliation.player == otherAffiliation.player) {
                    continue
                }

                val collisionPoint = entity collisionWith other
                if (collisionPoint != null) {
                    handleCollision(entity, other, collisionPoint)
                }
            }
        }
    }

    private fun handleCollision(e1: Entity, e2: Entity, at: Vector, flip: Boolean = true) {
        if (e1 hasEnabled CollisionDamageComponent::class && e2 hasEnabled HealthComponent::class) {
            val damage = e1.getEnabled(CollisionDamageComponent::class)!!
            val health = e2.getEnabled(HealthComponent::class)!!

            health.current -= damage.value

            engine.createLater(Explosion(at))
        }

        if (e1.getEnabled(CollisionComponent::class)!!.destroyOnCollision && e2 hasEnabled UnitComponent::class) {
            e1.getEnabled(CollisionComponent::class)!!.enabled = false
            engine.destroyLater(e1.id)
        }

        if (flip) {
            handleCollision(e2, e1, at, flip = false)
        }
    }
}

infix fun Entity.collisionWith(other: Entity): Vector? {
    val (t1, c1) = this.getEnabled(TransformComponent::class, CollisionComponent::class) ?: return null
    val (t2, c2) = other.getEnabled(TransformComponent::class, CollisionComponent::class) ?: return null

    return if (c1.layer != c2.layer) {
        null
    } else {
        (c1.mask at t1) intersectionWith (c2.mask at t2)
    }
}
