package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.*
import org.warlords.castlewar.sessionserver.utility.sq
import kotlin.math.sqrt

class AttackSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            if (!entity.enabled) {
                continue
            }

            val (cooldown, target, attacker) = entity.getEnabled(
                CooldownComponent::class, TargetComponent::class, AttackerComponent::class
            ) ?: continue

            if (cooldown.timeLeft > 0) {
                cooldown.timeLeft -= engine.actualTimePeriod
                continue
            }
            if (!engine.exists(target.id)) {
                continue
            }

            val (transform, affiliation) = entity.getEnabled(TransformComponent::class, AffiliationComponent::class)
                ?: continue

            val distanceSq = transform.position distanceToSq target.current

            if (attacker.minRange.sq <= distanceSq && distanceSq <= attacker.maxRange.sq) {
                val diff = ((target.current - transform.position).normalized() - transform.direction).lengthSq

                when (attacker.attackType) {
                    AntiAircraftMissile::class -> {
                        if (diff > 0.3) {
                            continue
                        }
                        val missileId = engine.createLater(
                            AntiAircraftMissile(
                                transform.position, transform.rotation,
                                target.id, target.current, affiliation.player
                            )
                        )
                        engine.runAfter(AntiAircraftMissile.SEEK_TIME) {
                            val missile = engine.getById(missileId)
                            if (missile != null) {
                                missile.get(RotateComponent::class)!!.enabled = false
                            }
                        }
                    }
                    Bomb::class -> {
                        if (diff > 1e-1) {
                            continue
                        }
                        val attackerSpeed = entity.getEnabled(SpeedComponent::class) ?: continue
                        var bombSpeed = attackerSpeed.speed * BOMB_SPEED_MULTIPLIER
                        val bombId = engine.createLater(
                            Bomb(transform.position, transform.rotation, bombSpeed, affiliation.player)
                        )
                        var bombFlightTime = (sqrt(distanceSq) * engine.timeUnit / (bombSpeed)).toInt()
                        if (bombSpeed < BOMB_SPEED_THRESHOLD) {
                            bombSpeed /= 5
                            bombFlightTime = 200
                        }

                        engine.runAfter(bombFlightTime) {
                            val bomb = engine.getById(bombId)
                            if (bomb != null) {
                                bomb.get(CollisionComponent::class)!!.enabled = true
                                bomb.get(LifetimeComponent::class)!!.enabled = true
                            }
                        }
                    }
                    Shell::class -> {
                        if (diff > 1e-4) {
                            continue
                        }
                        engine.createLater(
                            Shell(transform.position, transform.rotation, affiliation.player)
                        )
                    }
                }
                cooldown.timeLeft = cooldown.cooldown
            }
        }
    }

    companion object {

        private const val BOMB_SPEED_MULTIPLIER = 0.8
        private const val BOMB_SPEED_THRESHOLD = 20
    }
}