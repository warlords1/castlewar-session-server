package org.warlords.castlewar.sessionserver.ecs.components

import org.warlords.castlewar.sessionserver.ecs.base.Component
import org.warlords.castlewar.sessionserver.ecs.entities.attacks.Attack
import kotlin.reflect.KClass

class AttackerComponent(
    val attackType: KClass<out Attack>?,
    val minRange: Double, val maxRange: Double,
    val stopInRange: Boolean = true,
    val attackDamage: Int? = null
) : Component()
