package org.warlords.castlewar.sessionserver.ecs.entities.units

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.entities.Parameters
import org.warlords.castlewar.sessionserver.ecs.entities.misc.TankTurret
import org.warlords.castlewar.sessionserver.ecs.components.*
import org.warlords.castlewar.sessionserver.ecs.systems.GroundMovementSystem
import org.warlords.castlewar.sessionserver.ecs.systems.LineOfSightTargetSystem
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.GROUND_LAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class Tank(
    position: Vector, rotation: Double = 0.0,
    targetId: Int, targetPosition: Vector,
    player: Player
) : Unit(
    TransformComponent(position, rotation),
    CollisionComponent(CircleMask(COLLISION_RADIUS), GROUND_LAYER),
    AffiliationComponent(player),
    TargetComponent(targetId, targetPosition, TARGET_TYPES, resetTime = TARGET_RESET_TIME),
    UnitComponent(UnitType.Ground),
    HealthComponent(MAX_HEALTH),
    AttackerComponent(null, 0.0, SIGHT_DISTANCE * 0.5),
    LineOfSightComponent(SIGHT_DISTANCE, CONE_OF_VIEW),
    SpeedComponent(SPEED),
    RotateComponent(ROTATE_SPEED)
) {

    override val entityTypeName: String
        get() = "Tank"

    override fun onCreated(engine: Engine) {
        val tf = get(TransformComponent::class)!!
        engine.runLater {
            engine.createNow(
                TankTurret(
                    tf.position, tf.rotation, id, get(AffiliationComponent::class)!!.player
                )
            )
        }
    }

    override fun getSystems(): Array<KClass<out System>> = systems

    companion object {

        private val SPEED: Double = Parameters["Unit"]["Tank"]["speed"] as Double
        private val ROTATE_SPEED: Double = Parameters["Unit"]["Tank"]["rotate_speed"] as Double

        val MAX_HEALTH: Int = Parameters["Unit"]["Tank"]["max_health"] as Int

        val COLLISION_RADIUS: Double = Parameters["Unit"]["Tank"]["collision_radius"] as Double

        private val SIGHT_DISTANCE: Double = Parameters["Unit"]["Tank"]["sight_distance"] as Double
        private val CONE_OF_VIEW: Double = Parameters["Unit"]["Tank"]["cone_of_view"] as Double

        private val TARGET_RESET_TIME: Int = Parameters["Unit"]["Tank"]["target_reset_time"] as Int

        private val TARGET_TYPES = (Parameters["Unit"]["Tank"]["target_types"] as List<*>)
            .map { UnitType.valueOf(it as String) }
            .toTypedArray()

        val systems = Unit.systems + arrayOf(
            GroundMovementSystem::class,
            LineOfSightTargetSystem::class
        )
    }
}