package org.warlords.castlewar.sessionserver.ecs.systems

import org.warlords.castlewar.sessionserver.ecs.base.*
import org.warlords.castlewar.sessionserver.ecs.components.TargetComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.components.BindComponent

class TurretBindSystem : System() {

    override fun update(elapsedTime: Int, entities: Collection<Entity>) {
        for (entity in entities) {
            val (transform, binding, target) = entity.getEnabled(
                TransformComponent::class, BindComponent::class, TargetComponent::class
            ) ?: continue
            val parent = engine.getById(binding.id)
            if (parent == null) {
                engine.destroyLater(entity.id)
                continue
            }

            val (parentTransform, parentTarget) = parent.getEnabled(
                TransformComponent::class, TargetComponent::class
            ) ?: continue

            transform.position.setCoordinates(parentTransform.position)

            target.id = parentTarget.id
            target.current = parentTarget.current
        }
    }
}