package org.warlords.castlewar.sessionserver.utility

import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent

interface CollisionMask

class CircleMask(val radius: Double) : CollisionMask

class LineSegmentMask(val end: Vector) : CollisionMask

const val GROUND_LAYER = 0
const val AIR_LAYER = 1

infix fun CollisionMask.at(tf: TransformComponent) = MaskAtPosition(this, tf)

class MaskAtPosition(val mask: CollisionMask, val tf: TransformComponent)

infix fun MaskAtPosition.intersectionWith(other: MaskAtPosition): Vector? {
    return when {
        this.mask is CircleMask && other.mask is CircleMask ->
            intersection(this.tf.position, this.mask.radius, other.tf.position, other.mask.radius)
        this.mask is LineSegmentMask && other.mask is LineSegmentMask ->
            intersection(
                this.tf.position, this.mask.end.rotated(this.tf.rotation),
                other.tf.position, other.mask.end.rotated(other.tf.rotation)
            )
        this.mask is CircleMask && other.mask is LineSegmentMask -> {
            intersection(
                this.tf.position, this.mask.radius,
                other.tf.position, other.mask.end.rotated(other.tf.rotation)
            )
        }
        other.mask is CircleMask && this.mask is LineSegmentMask ->
            intersection(other.tf.position, other.mask.radius, this.tf.position, this.mask.end.rotated(this.tf.rotation))
        else -> null
    }
}

private fun intersection(c1: Vector, r1: Double, c2: Vector, r2: Double): Vector? {
    val diff = c2 - c1
    return if (diff.lengthSq <= (r1 + r2).sq) {
        c1 + diff * r1 / (r1 + r2)
    } else {
        null
    }
}

private fun Vector.distToSegmentSq(
    l: Vector,
    d: Vector
): Pair<Double, Vector> {
    val lineDist = d.lengthSq
    if (lineDist == 0.0) {
        return this distanceToSq l to l
    }
    val l2 = l + d
    var t = ((x - l.x) * (l2.x - l.x) + (y - l.y) * (l2.y - l.y)) / lineDist
    t = constrain(t)
    val point = Vector(l.x + t * d.x, l.y + t * d.y)
    val distance = this distanceToSq point
    return distance to point
}

private fun intersection(p1: Vector, d1: Vector, p2: Vector, d2: Vector): Vector? {
    val b = p1 + d1
    val d = p2 + d2
    val cmp = p2 - p1
    val r = b - p1
    val s = d - p2

    val cmpXr = cmp.x * r.y - cmp.y * r.x
    val cmpXs = cmp.x * s.y - cmp.y * s.x
    val rxs = r.x * s.y - r.y * s.x

    if (cmpXr == 0.0) {
        // Lines are collinear, and so intersect if they have any overlap
        return p1 + (d1 * 0.5)
    }

    if (rxs == 0.0) {
        return null
    }

    val rXsr = 1.0 / rxs
    val t = cmpXs * rXsr
    val u = cmpXr * rXsr

    return if ((t >= 0.0) && (t <= 1.0) && (u >= 0.0) && (u <= 1.0)) {
        p1 + d1 * t
    } else {
        null
    }
}

private fun intersection(c: Vector, r: Double, p1: Vector, d1: Vector): Vector? {
    val (distanceSq, point) = c.distToSegmentSq(p1, d1)
    return if (distanceSq <= r.sq) {
        point
    } else {
        null
    }
}
