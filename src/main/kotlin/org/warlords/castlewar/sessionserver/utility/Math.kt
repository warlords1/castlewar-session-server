package org.warlords.castlewar.sessionserver.utility

import kotlin.math.roundToInt

fun constrain(t: Double): Double {
    return when {
        t < 0.0 -> 0.0
        t > 1.0 -> 1.0
        else -> t
    }
}

val Double.sq
    get() = this * this

fun Double.round(other: Double): Double = other * (this / other).roundToInt()
