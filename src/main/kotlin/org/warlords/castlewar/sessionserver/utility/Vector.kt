package org.warlords.castlewar.sessionserver.utility

import kotlin.math.*

class Vector(var x: Double, var y: Double) {

    val lengthSq: Double
        get() = x * x + y * y

    val length: Double
        get() = sqrt(lengthSq)

    override fun toString() = "($x, $y)"

    fun copy(): Vector = Vector(x, y)

    operator fun minus(other: Vector): Vector = Vector(this.x - other.x, this.y - other.y)

    operator fun plus(other: Vector): Vector = Vector(this.x + other.x, this.y + other.y)

    operator fun minusAssign(other: Vector) {
        x -= other.x
        y -= other.y
    }

    operator fun plusAssign(other: Vector) {
        x += other.x
        y += other.y
    }

    operator fun times(alpha: Double): Vector = Vector(x * alpha, y * alpha)

    operator fun timesAssign(alpha: Double) {
        x *= alpha
        y *= alpha
    }

    operator fun div(alpha: Double): Vector = Vector(x / alpha, y / alpha)

    operator fun divAssign(alpha: Double) {
        x /= alpha
        y /= alpha
    }

    infix fun distanceTo(other: Vector): Double = sqrt(this distanceToSq other)

    infix fun distanceToSq(other: Vector): Double = (x - other.x).sq + (y - other.y).sq

    fun setCoordinates(x: Double, y: Double) {
        this.x = x
        this.y = y
    }

    fun setCoordinates(other: Vector) {
        x = other.x
        y = other.y
    }

    fun normalize() {
        val l = length
        if (l == 0.0) {
            x = 1.0
            y = 0.0
        }
        this /= length
    }

    fun normalized(): Vector {
        val l = length
        if (l == 0.0) {
            return Vector(1.0, 0.0)
        }
        return copy() / length
    }

    fun angle() = this.angleRelativeTo(e)

    private fun rotate(angle: Double) {
        val xx = x
        val yy = y
        x = xx * cos(angle) - yy * sin(angle)
        y = xx * sin(angle) + yy * cos(angle)
    }

    fun rotated(angle: Double): Vector {
        val c = copy()
        c.rotate(angle)
        return c
    }

    fun angleRelativeTo(other: Vector): Double = atan2(this.y, this.x) - atan2(other.y, other.x)

    override fun equals(other: Any?) = other is Vector && other.x == x && other.y == y

    override fun hashCode() = x.hashCode() * 31 + y.hashCode()

    companion object {

        val e
            get() = Vector(1.0, 0.0)
        val zero
            get() = Vector(0.0, 0.0)
    }
}
