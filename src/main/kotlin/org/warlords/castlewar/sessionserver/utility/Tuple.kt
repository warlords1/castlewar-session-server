package org.warlords.castlewar.sessionserver.utility

data class Tuple2<T1, T2>(
    val item1: T1,
    val item2: T2
)

data class Tuple3<T1, T2, T3>(
    val item1: T1,
    val item2: T2,
    val item3: T3
)

data class Tuple4<T1, T2, T3, T4>(
    val item1: T1,
    val item2: T2,
    val item3: T3,
    val item4: T4
)

data class Tuple5<T1, T2, T3, T4, T5>(
    val item1: T1,
    val item2: T2,
    val item3: T3,
    val item4: T4,
    val item5: T5
)
