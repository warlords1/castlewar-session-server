package org.warlords.castlewar.sessionserver.utility

typealias Player = Boolean

const val LEFT_PLAYER = false
const val RIGHT_PLAYER = true

fun Player.repr() = if (this == LEFT_PLAYER) "Left Player" else "Right Player"

fun Player.other() = !this
