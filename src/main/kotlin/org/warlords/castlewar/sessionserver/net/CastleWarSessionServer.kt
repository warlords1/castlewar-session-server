package org.warlords.castlewar.sessionserver.net

import io.javalin.Javalin
import io.javalin.websocket.WsContext
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.Player
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER
import java.nio.ByteBuffer

class CastleWarSessionServer(private val port: Int) {

    private var leftPlayerConn: WsContext? = null
    private var leftConnected: Boolean = false

    private var rightPlayerConn: WsContext? = null
    private var rightConnected: Boolean = false

    private var onConnectedHandler: ((Player) -> Unit)? = null
    private var onDisconnectedHandler: ((Player) -> Unit)? = null
    private var onMessageHandler: ((Player, ByteArray) -> Unit)? = null

    private val app = Javalin.create { config ->
        config.enableCorsForAllOrigins()
    }.apply {
        wsBefore(ENDPOINT) { ws ->
            ws.onConnect { ctx -> onConnected(ctx) }
            ws.onClose { ctx -> onDisconnected(ctx) }
        }
        ws(ENDPOINT) { ws ->
            ws.onConnect { ctx ->
                if (ctx.session.isOpen) {
                    getPlayer(ctx)?.let { player ->
                        onConnectedHandler?.invoke(player)
                    }
                }
            }
            ws.onBinaryMessage { ctx ->
                getPlayer(ctx)?.let { player ->
                    onMessageHandler?.invoke(player, ctx.data())
                }
            }
            ws.onClose { ctx ->
                getPlayer(ctx)?.let { player ->
                    onDisconnectedHandler?.invoke(player)
                }
            }
        }
    }

    @Synchronized
    private fun getPlayer(ctx: WsContext): Player? = when (ctx) {
        leftPlayerConn -> LEFT_PLAYER
        rightPlayerConn -> RIGHT_PLAYER
        else -> null
    }

    @Synchronized
    private fun onConnected(ctx: WsContext) = when {
        null == leftPlayerConn -> {
            leftPlayerConn = ctx
            leftConnected = true
        }
        null == rightPlayerConn -> {
            rightPlayerConn = ctx
            rightConnected = true
        }
        else -> ctx.session.disconnect()
    }

    @Synchronized
    private fun onDisconnected(ctx: WsContext) {
        when (ctx) {
            leftPlayerConn -> leftConnected = false
            rightPlayerConn -> rightConnected = false
        }
    }

    @Synchronized
    fun bothPlayersConnected() = leftConnected and rightConnected

    @Synchronized
    fun neitherPlayerConnected() = !leftConnected and !rightConnected

    fun start(): CastleWarSessionServer {
        app.start(port)
        return this
    }

    fun stop(): CastleWarSessionServer {
        app.stop()
        return this
    }

    fun onConnected(action: (Player) -> Unit) {
        onConnectedHandler = action
    }

    fun onMessage(action: (Player, ByteArray) -> Unit) {
        onMessageHandler = action
    }

    fun onDisconnected(action: (Player) -> Unit) {
        onDisconnectedHandler = action
    }

    fun send(player: Player, data: ByteArray) {
        val ctx = when (player) {
            LEFT_PLAYER -> leftPlayerConn
            RIGHT_PLAYER -> rightPlayerConn
            else -> throw IllegalArgumentException("player?")
        }
        ctx?.let {
            if (it.session.isOpen) {
                it.send(ByteBuffer.wrap(data))
            }
        }
    }

    companion object {

        const val ENDPOINT = "/castlewar"
    }
}