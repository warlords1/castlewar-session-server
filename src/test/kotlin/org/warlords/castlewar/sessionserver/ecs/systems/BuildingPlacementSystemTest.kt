package org.warlords.castlewar.sessionserver.ecs.systems

import org.junit.Assert.*
import org.junit.Test
import org.warlords.castlewar.sessionserver.CELL_SIZE
import org.warlords.castlewar.sessionserver.HEIGHT
import org.warlords.castlewar.sessionserver.WIDTH
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.AffiliationComponent
import org.warlords.castlewar.sessionserver.ecs.components.CollisionComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.buildings.*
import org.warlords.castlewar.sessionserver.utility.CircleMask
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.reflect.KClass

class BuildingPlacementSystemTest {

    @Test
    fun instantiateBuilding() {
        val classes = arrayOf(
            HQ::class,
            TankFactory::class,
            LightTankFactory::class,
            FighterAirfield::class,
            BomberAirfield::class,
            Artillery::class,
            SAM::class
        )

        for (cls in classes) {
            val building = instantiateBuilding(cls, Vector.zero, LEFT_PLAYER)
            assertNotNull(building)
            building!!
            assertEquals(cls, building::class)
        }

        assertNull(instantiateBuilding(dummyBuilding()::class, Vector.zero, LEFT_PLAYER))
    }

    @Test
    fun placeBuildingBadPosition() {
        val (engine, placementSystem, economicsSystem) = setup()
        economicsSystem.minerals[LEFT_PLAYER] = 9999

        val badPositions = arrayOf(
            Vector(-1.0, 1.0) to LEFT_PLAYER,
            Vector(1.0, -1.0) to LEFT_PLAYER,
            Vector(WIDTH + 1.0, 1.0) to LEFT_PLAYER,
            Vector(1.0, HEIGHT + 1.0) to LEFT_PLAYER,
            Vector(WIDTH - 1.0, HEIGHT / 2) to LEFT_PLAYER,
            Vector(1.0, HEIGHT / 2) to RIGHT_PLAYER,
        )
        badPositions.forEach { (at, player) ->
            val result = placementSystem.placeBuilding(HQ::class, at, player)
            assertEquals("position", result)
        }

        var result = placementSystem.placeBuilding(HQ::class, Vector(WIDTH / 4, HEIGHT / 2), LEFT_PLAYER)
        assertEquals("ok", result)
        repeat(2) { engine.update(0) }
        result = placementSystem.placeBuilding(HQ::class, Vector(WIDTH / 4, HEIGHT / 2), LEFT_PLAYER)
        assertEquals("position", result)
    }

    @Test
    fun placeBuildingNoResources() {
        val (_, placementSystem, _) = setup()
        val result = placementSystem.placeBuilding(HQ::class, Vector(WIDTH / 4, HEIGHT / 2), LEFT_PLAYER)
        assertEquals("resources", result)
    }

    @Test
    fun placeBuildingNoCostInfo() {
        val (_, placementSystem, _) = setup()
        val result = placementSystem.placeBuilding(dummyBuilding()::class, Vector(WIDTH / 4, HEIGHT / 2), LEFT_PLAYER)
        assertEquals("server_cost", result)
    }

    @Test
    fun getBuilding() {
        val (engine, placementSystem, economicsSystem) = setup()
        economicsSystem.minerals[LEFT_PLAYER] = 9999

        val p1 = Vector(WIDTH / 4, HEIGHT / 4 * 3)
        val p2 = Vector(WIDTH / 4, HEIGHT / 4)
        val p3 = Vector(WIDTH / 4, HEIGHT / 2)

        placementSystem.placeBuilding(HQ::class, p1, LEFT_PLAYER)
        placementSystem.placeBuilding(SAM::class, p2, LEFT_PLAYER)
        repeat(2) { engine.update(0) }

        val b1 = placementSystem.getBuilding(p1.x, p1.y)
        assertNotNull(b1)
        assertEquals(HQ::class, b1!!::class)

        val b2 = placementSystem.getBuilding(p2.x, p2.y)
        assertNotNull(b2)
        assertEquals(SAM::class, b2!!::class)

        val b3 = placementSystem.getBuilding(p3.x, p3.y)
        assertNull(b3)
    }

    companion object {

        fun setup(): Triple<Engine, BuildingPlacementSystem, EconomicsSystem> {
            val placementSystem = BuildingPlacementSystem(WIDTH, HEIGHT, CELL_SIZE)
            val economicsSystem = EconomicsSystem()
            val engine = Engine(systems = arrayOf(placementSystem, economicsSystem))

            return Triple(engine, placementSystem, economicsSystem)
        }

        fun dummyBuilding() = object : Building(
            TransformComponent(Vector.zero),
            CollisionComponent(CircleMask(0.0)),
            AffiliationComponent(LEFT_PLAYER),
            HealthComponent(0)
        ) {
            override val entityTypeName = ""
            override fun getSystems(): Array<KClass<out System>> = emptyArray()
        }
    }
}