package org.warlords.castlewar.sessionserver.ecs.systems

import org.junit.Test

import org.junit.Assert.*
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.components.RotateComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.buildings.HQ
import org.warlords.castlewar.sessionserver.ecs.entities.units.FighterPlane
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.math.PI

class SeekerSystemTest {

    @Test
    fun update() {
        val engine = Engine(systems = arrayOf(SeekerSystem()))
        val target = Vector(-1.0, 1.0)
        val entity = FighterPlane(Vector.zero, 0.0, Entity.NO_ID, target, LEFT_PLAYER)
        engine.createNow(entity)
        engine.createNow(HQ(Vector.zero, LEFT_PLAYER), SeekerSystem::class)
        var timeMs = 0
        repeat(10) {
            timeMs += Engine.DEFAULT_UPDATE_PERIOD
            engine.update(timeMs)
        }
        assertEquals(target.angle(), entity.get(TransformComponent::class)!!.rotation, 1e-5)
    }

    @Test
    fun rotateTowardsObject() {
        val tf = TransformComponent(Vector.zero, rotation = 0.0)
        val targets = arrayOf(
            Vector(1.0, 1.0),
            Vector(1.0, -1.0),
            Vector(-1.0, 1.0),
            Vector(-1.0, -1.0),
        )

        var rt = RotateComponent()
        for (target in targets) {
            tf.rotation = 0.0
            tf.rotateTowardsObject(target, rt, 1.0)
            assertEquals(target.angle(), tf.rotation, 1e-3)
        }

        rt = RotateComponent(PI)
        for (target in targets) {
            tf.rotation = 0.0
            tf.rotateTowardsObject(target, rt, 1.0)
            assertEquals(target.angle(), tf.rotation, 1e-3)
        }

        rt = RotateComponent(PI / 8)
        for (target in targets) {
            tf.rotation = 0.0
            repeat(8) { tf.rotateTowardsObject(target, rt, 1.0) }
            assertEquals(target.angle(), tf.rotation, 1e-5)
        }
    }
}