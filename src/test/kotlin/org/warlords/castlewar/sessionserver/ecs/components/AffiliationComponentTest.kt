package org.warlords.castlewar.sessionserver.ecs.components

import org.junit.Assert.*
import org.junit.Test
import org.warlords.castlewar.sessionserver.utility.LEFT_PLAYER
import org.warlords.castlewar.sessionserver.utility.RIGHT_PLAYER

class AffiliationComponentTest {

    @Test
    fun init() {
        assertEquals(LEFT_PLAYER, AffiliationComponent(LEFT_PLAYER).player)
        assertEquals(RIGHT_PLAYER, AffiliationComponent(RIGHT_PLAYER).player)
    }
}