package org.warlords.castlewar.sessionserver.ecs.base

import org.junit.Test

import org.junit.Assert.*
import kotlin.reflect.KClass

class ComponentTest {

    @Test
    fun entity() {
        val c = object : Component() {}
        assertThrows(NullPointerException::class.java) { c.entity }

        c.internalEntity = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertNotNull(c.entity)
    }

    @Test
    fun hasEntity() {
        val c = object : Component() {}
        assertFalse(c.hasEntity)

        c.internalEntity = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertTrue(c.hasEntity)
    }

    @Test
    fun enabled() {
        var c = object : Component() {} as Component
        assertTrue(c.enabled)
        c.enabled = false
        assertFalse(c.enabled)
        c = object : Component(enabled = false) {}
        assertFalse(c.enabled)
    }

}