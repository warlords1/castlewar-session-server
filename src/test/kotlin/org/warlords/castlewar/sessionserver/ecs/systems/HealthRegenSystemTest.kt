package org.warlords.castlewar.sessionserver.ecs.systems

import org.junit.Test

import org.junit.Assert.*
import org.warlords.castlewar.sessionserver.ecs.base.Engine
import org.warlords.castlewar.sessionserver.ecs.base.Entity
import org.warlords.castlewar.sessionserver.ecs.base.System
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthRegenComponent
import kotlin.reflect.KClass

class HealthRegenSystemTest {

    @Test
    fun update() {
        val engine = Engine(systems = arrayOf(HealthRegenSystem()))
        val maxHealth = 100
        val regenRate = 10
        val entity = object : Entity(HealthComponent(maxHealth), HealthRegenComponent(regenRate)) {
            override val entityTypeName = ""
            override fun getSystems(): Array<KClass<out System>> = arrayOf(HealthRegenSystem::class)
        }
        val health = entity.getEnabled(HealthComponent::class)!!
        engine.createNow(entity)
        assertEquals(maxHealth, health.current)
        health.current /= 2
        assertEquals(maxHealth/2, health.current)
        var timeMs = 0
        repeat(maxHealth / 2 / regenRate) {
            timeMs += Engine.DEFAULT_UPDATE_PERIOD
            engine.update(timeMs)
        }
        assertEquals(maxHealth, health.current)
        engine.update(timeMs + Engine.DEFAULT_UPDATE_PERIOD)
        assertEquals(maxHealth, health.current)
    }

    @Test
    fun noComponent() {
        val engine = Engine(systems = arrayOf(HealthRegenSystem()))
        val maxHealth = 100
        val regenRate = 10
        val entity = object : Entity(HealthComponent(maxHealth), HealthRegenComponent(regenRate)) {
            override val entityTypeName = ""
            override fun getSystems(): Array<KClass<out System>> = arrayOf(HealthRegenSystem::class)
        }
        val health = entity.getEnabled(HealthComponent::class)!!
        health.enabled = false
        engine.createNow(entity)
        assertEquals(maxHealth, health.current)
        health.current /= 2
        assertEquals(maxHealth/2, health.current)
        var timeMs = 0
        repeat(maxHealth / 2 / regenRate) {
            timeMs += Engine.DEFAULT_UPDATE_PERIOD
            engine.update(timeMs)
        }
        assertEquals(maxHealth / 2, health.current)
    }
}