package org.warlords.castlewar.sessionserver.ecs.components

import org.junit.Assert.*
import org.junit.Test
import org.warlords.castlewar.sessionserver.utility.Vector
import kotlin.math.PI

class TransformComponentTest {

    @Test
    fun init() {
        val t = TransformComponent(Vector(1.0, 2.0), PI / 2)
        assertEquals(1.0, t.position.x, 0.0)
        assertEquals(2.0, t.position.y, 0.0)
        assertEquals(PI / 2, t.rotation, 1e-6)
    }

    @Test
    fun direction() {
        val t = TransformComponent(Vector(1.0, 2.0), PI / 2)
        val d = t.direction
        assertEquals(0.0, d.x, 1e-5)
        assertEquals(1.0, d.y, 1e-5)
    }
}