package org.warlords.castlewar.sessionserver.ecs.components

import org.junit.Test

import org.junit.Assert.*

class HealthComponentTest {

    @Test
    fun current() {
        val max = 100
        val c = HealthComponent(max)
        assertEquals(max, c.max)
        assertEquals(max, c.current)

        c.current = 50
        assertEquals(50, c.current)
        c.current = -50
        assertEquals(0, c.current)
    }

    @Test
    fun getAlive() {
        val max = 100
        val c = HealthComponent(max)
        assertTrue(c.alive)
        c.current = -10
        assertFalse(c.alive)
    }
}