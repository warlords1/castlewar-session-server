package org.warlords.castlewar.sessionserver.ecs.base

import org.junit.Test

class SystemTest {

    @Test
    fun engine() {
        val s = object : System() {
            override fun update(elapsedTime: Int, entities: Collection<Entity>) = Unit
        }
        s.engine = Engine()
    }

    @Test
    fun update() {
        val s = object : System() {
            override fun update(elapsedTime: Int, entities: Collection<Entity>) = Unit
        }
        s.update(0, emptyList())
    }
}