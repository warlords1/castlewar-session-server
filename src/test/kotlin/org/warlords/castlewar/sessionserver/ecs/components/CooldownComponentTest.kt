package org.warlords.castlewar.sessionserver.ecs.components

import org.junit.Assert.*
import org.junit.Test

class CooldownComponentTest {

    @Test
    fun timeLeft() {
        val max = 100
        var c = CooldownComponent(max)
        assertEquals(max, c.timeLeft)

        c = CooldownComponent(max, readyOnStart = true)
        assertEquals(0, c.timeLeft)

        c.timeLeft = max
        assertEquals(max, c.timeLeft)
    }
}