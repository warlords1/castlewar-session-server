package org.warlords.castlewar.sessionserver.ecs.base

import org.junit.Assert.*
import org.junit.Test
import org.warlords.castlewar.sessionserver.ecs.components.AffiliationComponent
import org.warlords.castlewar.sessionserver.ecs.components.CooldownComponent
import org.warlords.castlewar.sessionserver.ecs.components.HealthComponent
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import org.warlords.castlewar.sessionserver.ecs.entities.EntityTypeId
import org.warlords.castlewar.sessionserver.ecs.systems.DeathSystem
import org.warlords.castlewar.sessionserver.utility.*
import kotlin.reflect.KClass

class EntityTest {

    @Test
    fun nextId() {
        assertNotEquals(Entity.nextId, Entity.nextId)
        val id = 42
        Entity.nextId = id
        assertNotEquals(id, Entity.nextId)
        Entity.nextId = Entity.NO_ID
        assertNotEquals(Entity.NO_ID, Entity.nextId)
    }

    @Test
    fun bindComponents() {
        val c = object : Component() {}
        val e = object : Entity(c) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertFalse(c.hasEntity)
        e.bindComponents()
        assertTrue(c.hasEntity)
        assertEquals(e, c.entity)
    }

    @Test
    fun id() {
        val e = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertNotEquals(Entity.NO_ID, e.id)
    }

    @Test
    fun hasEnabled() {
        val c = HealthComponent(100)
        val e = object : Entity(c) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }

        assertTrue(e hasEnabled HealthComponent::class)
        assertTrue(e hasEnabled HealthComponent::class)
        c.enabled = false
        assertFalse(e hasEnabled HealthComponent::class)
    }

    @Test
    fun getEnabled() {
        val c = HealthComponent(100)
        val e = object : Entity(c) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }

        assertEquals(c, e.getEnabled(HealthComponent::class))
        c.enabled = false
        assertNull(e.getEnabled(HealthComponent::class))

        c.enabled = true
        assertEquals(Tuple2(c, c), e.getEnabled(HealthComponent::class, HealthComponent::class))
        assertEquals(
            Tuple3(c, c, c),
            e.getEnabled(HealthComponent::class, HealthComponent::class, HealthComponent::class)
        )
        assertEquals(
            Tuple4(c, c, c, c),
            e.getEnabled(HealthComponent::class, HealthComponent::class, HealthComponent::class, HealthComponent::class)
        )
        assertEquals(
            Tuple5(c, c, c, c, c),
            e.getEnabled(
                HealthComponent::class,
                HealthComponent::class,
                HealthComponent::class,
                HealthComponent::class,
                HealthComponent::class
            )
        )

        assertNull(e.getEnabled(TransformComponent::class))

        assertNull(e.getEnabled(TransformComponent::class, TransformComponent::class))
        assertNull(e.getEnabled(HealthComponent::class, TransformComponent::class))

        assertNull(e.getEnabled(TransformComponent::class, TransformComponent::class, TransformComponent::class))
        assertNull(e.getEnabled(HealthComponent::class, TransformComponent::class, TransformComponent::class))
        assertNull(e.getEnabled(HealthComponent::class, HealthComponent::class, TransformComponent::class))

        assertNull(
            e.getEnabled(
                TransformComponent::class,
                TransformComponent::class,
                TransformComponent::class,
                TransformComponent::class
            )
        )
        assertNull(
            e.getEnabled(
                TransformComponent::class,
                TransformComponent::class,
                TransformComponent::class,
                TransformComponent::class,
                TransformComponent::class
            )
        )
    }

    @Test
    fun get() {
        val c = HealthComponent(100)
        val e = object : Entity(c) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }

        assertEquals(c, e.get(HealthComponent::class))
        assertEquals(c, e.get(HealthComponent::class))
        c.enabled = false
        assertEquals(c, e.get(HealthComponent::class))

        assertNull(e.get(TransformComponent::class))
    }

    @Test
    fun getAllEnabled() {
        val c = listOf(HealthComponent(100), HealthComponent(200))
        val e = object : Entity(*c.toTypedArray()) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }

        assertEquals(c, e.getAllEnabled(HealthComponent::class))
        assertEquals(c, e.getAllEnabled(HealthComponent::class))
        c[0].enabled = false
        assertEquals(listOf(c[1]), e.getAllEnabled(HealthComponent::class))
    }

    @Test
    fun getAll() {
        val c = listOf(HealthComponent(100), HealthComponent(200))
        val e = object : Entity(*c.toTypedArray()) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }

        assertEquals(c, e.getAll(HealthComponent::class))
        c[0].enabled = false
        assertEquals(c, e.getAll(HealthComponent::class))
    }

    @Test
    fun getSystems() {
        var e = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        } as Entity
        assertTrue(e.getSystems().isEmpty())

        e = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems(): Array<KClass<out System>> = arrayOf(DeathSystem::class)
        }
        assertArrayEquals(arrayOf<KClass<out System>>(DeathSystem::class), e.getSystems())
    }

    @Test
    fun serialize() {
        var data = object : Entity(TransformComponent(0.0, 0.0)) {
            override val entityTypeName = "HQ"
            override fun getSystems() = emptyArray<KClass<out System>>()
        }.serialize()
        assertEquals(EntityTypeId["HQ"], data.typeId)
        assertEquals(0.0, data.transform.x, 0.0)
        assertEquals(0.0, data.transform.y, 0.0)
        assertEquals(0.0, data.transform.rotation, 0.0)

        data = object : Entity(
            TransformComponent(0.0, 0.0),
            CooldownComponent(100)
        ) {
            override val entityTypeName = "HQ"
            override fun getSystems() = emptyArray<KClass<out System>>()
        }.serialize()
        assertEquals(100, data.cooldown)

        data = object : Entity(
            TransformComponent(0.0, 0.0),
            AffiliationComponent(LEFT_PLAYER)
        ) {
            override val entityTypeName = "HQ"
            override fun getSystems() = emptyArray<KClass<out System>>()
        }.serialize()
        assertEquals(LEFT_PLAYER, data.player)

        data = object : Entity(
            TransformComponent(0.0, 0.0),
            HealthComponent(200)
        ) {
            override val entityTypeName = "HQ"
            override fun getSystems() = emptyArray<KClass<out System>>()
        }.serialize()
        assertEquals(200, data.health)
    }

    @Test
    fun testEquals() {
        val e1 = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        val e2 = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertNotEquals(e1, e2)
        assertNotEquals(null, e2)
        assertNotEquals(e1, null)
        assertNotEquals(e1, object : Any() {})
        assertEquals(e1, e1)
        assertEquals(e2, e2)
    }

    @Test
    fun testHashCode() {
        val e = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertEquals(e.id, e.hashCode())
    }

    @Test
    fun enabled() {
        var e = object : Entity() {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        } as Entity

        assertTrue(e.enabled)
        e = object : Entity(enabled = false) {
            override val entityTypeName = ""
            override fun getSystems() = emptyArray<KClass<out System>>()
        }
        assertFalse(e.enabled)
    }
}