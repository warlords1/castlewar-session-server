package org.warlords.castlewar.sessionserver.utility

import org.junit.Assert.*
import org.junit.Test

class TupleTest {

    @Test
    fun t2() {
        val t = Tuple2(1, 2)
        assertEquals(1, t.item1)
        assertEquals(2, t.item2)
    }

    @Test
    fun t3() {
        val t = Tuple3(1, 2, 3)
        assertEquals(1, t.item1)
        assertEquals(2, t.item2)
        assertEquals(3, t.item3)
    }

    @Test
    fun t4() {
        val t = Tuple4(1, 2, 3, 4)
        assertEquals(1, t.item1)
        assertEquals(2, t.item2)
        assertEquals(3, t.item3)
        assertEquals(4, t.item4)
    }

    @Test
    fun t5() {
        val t = Tuple5(1, 2, 3, 4, 5)
        assertEquals(1, t.item1)
        assertEquals(2, t.item2)
        assertEquals(3, t.item3)
        assertEquals(4, t.item4)
        assertEquals(5, t.item5)
    }
}