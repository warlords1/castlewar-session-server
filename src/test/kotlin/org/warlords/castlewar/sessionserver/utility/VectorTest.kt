package org.warlords.castlewar.sessionserver.utility

import org.junit.Assert.*
import org.junit.Test

import kotlin.math.PI

internal class VectorTest {

    @Test
    fun getLengthSq() {
        val v1 = Vector(0.0, 0.0)
        assertEquals(0.0, v1.lengthSq, 0.0)
        val v2 = Vector(3.0, 4.0)
        assertEquals(25.0, v2.lengthSq, 0.0)
    }

    @Test
    fun getLength() {
        val v1 = Vector(0.0, 0.0)
        assertEquals(0.0, v1.length, 0.0)
        val v2 = Vector(3.0, 4.0)
        assertEquals(5.0, v2.length, 1e-5)
    }

    @Test
    fun testToString() {
        val v1 = Vector(0.0, 0.0)
        assertEquals("(0.0, 0.0)", "$v1")
        val v2 = Vector(-1.7, 2.3)
        assertEquals("(-1.7, 2.3)", "$v2")
    }

    @Test
    fun copy() {
        val x = -1.7
        val y = 2.3
        val v1 = Vector(x, y)
        val v2 = v1.copy()
        assertEquals(v1, v2)
        v2.x = 0.0
        v2.y = -1.0
        assertEquals(x, v1.x, 0.0)
        assertEquals(y, v1.y, 0.0)
        assertNotEquals(v1, v2)
    }

    @Test
    fun minus() {
        val v1 = Vector(1.5, 2.5)
        val v2 = Vector(0.5, 1.5)
        val v3 = v1 - v2
        assertEquals(1.0, v3.x, 1e-5)
        assertEquals(1.0,v3.y,  1e-5)
    }

    @Test
    fun plus() {
        val v1 = Vector(1.5, 2.5)
        val v2 = Vector(0.5, 1.5)
        val v3 = v1 + v2
        assertEquals(2.0, v3.x, 1e-5)
        assertEquals(4.0, v3.y, 1e-5)
    }

    @Test
    fun minusAssign() {
        val v1 = Vector(1.5, 2.5)
        val v2 = Vector(0.5, 1.5)
        v1 -= v2
        assertEquals(1.0,v1.x,  1e-5)
        assertEquals(1.0, v1.y, 1e-5)
    }

    @Test
    fun plusAssign() {
        val v1 = Vector(1.5, 2.5)
        val v2 = Vector(0.5, 1.5)
        v1 += v2
        assertEquals(2.0,v1.x,  1e-5)
        assertEquals(4.0,v1.y,  1e-5)
    }

    @Test
    fun times() {
        val v1 = Vector(1.0, -2.0)
        val v2 = v1 * 2.5
        assertEquals(2.5,v2.x,  1e-5)
        assertEquals(-5.0,v2.y,  1e-5)
    }

    @Test
    fun timesAssign() {
        val v1 = Vector(1.0, -2.0)
        v1 *= 2.5
        assertEquals(2.5,v1.x,  1e-5)
        assertEquals(-5.0,v1.y,  1e-5)
    }

    @Test
    fun div() {
        val v1 = Vector(1.0, -2.0)
        val v2 = v1 / 2.0
        assertEquals( 0.5,v2.x, 1e-5)
        assertEquals(-1.0,v2.y,  1e-5)
    }

    @Test
    fun divAssign() {
        val v1 = Vector(1.0, -2.0)
        v1 /= 2.5
        assertEquals(0.4,v1.x,  1e-5)
        assertEquals(-0.8, v1.y, 1e-5)
    }

    @Test
    fun distanceTo() {
        val v1 = Vector(0.0, 3.0)
        val v2 = Vector(4.0, 0.0)
        assertEquals(5.0, v1 distanceTo v2, 1e-5)
    }

    @Test
    fun distanceToSq() {
        val v1 = Vector(0.0, 3.0)
        val v2 = Vector(4.0, 0.0)
        assertEquals(25.0, v1 distanceToSq v2, 1e-5)
    }

    @Test
    fun setCoordinates() {
        val x = -1.7
        val y = 2.3
        val v1 = Vector(0.0, 0.0)
        val v2 = Vector(x, y)
        v1.setCoordinates(x, y)
        assertEquals(v1, v2)
        v2 *= 2.0
        v1.setCoordinates(v2)
        assertEquals(v1, v2)
    }

    @Test
    fun normalize() {
        val v1 = Vector(3.0, 4.0)
        v1.normalize()
        assertEquals(0.6, v1.x, 1e-5)
        assertEquals(0.8, v1.y, 1e-5)

        val zero = Vector.zero
        zero.normalize()
        assertEquals(zero, Vector.e)
    }

    @Test
    fun normalized() {
        var v1 = Vector(3.0, 4.0)
        v1 = v1.normalized()
        assertEquals(0.6, v1.x, 1e-5)
        assertEquals(0.8, v1.y, 1e-5)

        val zero = Vector.zero
        assertEquals(zero.normalized(), Vector.e)
    }

    @Test
    fun angle() {
        val v1 = Vector(1.0, 1.0)
        assertEquals(PI / 4, v1.angle(), 1e-5)
        val v2 = Vector(-1.0, 1.0)
        assertEquals(PI / 4 * 3, v2.angle(), 1e-5)
    }

    @Test
    fun rotated() {
        val v1 = Vector.e.rotated(PI / 4)
        assertEquals(PI / 4, v1.angle(), 1e-5)
        assertEquals(1.0, v1.length, 1e-5)
    }

    @Test
    fun angleRelativeTo() {
        val v1 = Vector.e.rotated(PI / 2)
        val v2 = Vector.e.rotated(PI / 3)

        assertEquals(PI / 6, v1.angleRelativeTo(v2), 1e-5)
        assertEquals(-PI / 6, v2.angleRelativeTo(v1), 1e-5)
    }

    @Test
    fun testEquals() {
        val x = 0.5
        val y = -3.2
        assertTrue(Vector(x, y) == Vector(x, y))
        assertFalse(Vector(-x, y) == Vector(x, y))
        assertFalse(Vector(x, y) == Vector(x, -y))
        val nullVector = null
        assertFalse(Vector.e == nullVector)
    }

    @Test
    fun testHashCode() {
        val x = 0.5
        val y = -3.2
        assertEquals(Vector(x, y).hashCode(), Vector(x, y).hashCode())
    }

    @Test
    fun getX() {
        val x = 0.5
        val y = -3.2
        val v1 = Vector(x, y)
        assertEquals(x, v1.x, 0.0)
    }

    @Test
    fun setX() {
        val x = 0.5
        val y = -3.2
        val v1 = Vector(0.0, y)
        v1.x = x
        assertEquals(x, v1.x, 0.0)
    }

    @Test
    fun getY() {
        val x = 0.5
        val y = -3.2
        val v1 = Vector(x, y)
        assertEquals(y, v1.y, 0.0)
    }

    @Test
    fun setY() {
        val x = 0.5
        val y = -3.2
        val v1 = Vector(x, 0.0)
        v1.y = y
        assertEquals(y, v1.y, 0.0)
    }

    @Test
    fun e() {
        val e = Vector.e
        assertEquals(1.0, e.x, 0.0)
        assertEquals(0.0, e.y, 0.0)
    }

    @Test
    fun zero() {
        val zero = Vector.zero
        assertEquals(0.0, zero.x, 0.0)
        assertEquals(0.0, zero.y, 0.0)
    }
}