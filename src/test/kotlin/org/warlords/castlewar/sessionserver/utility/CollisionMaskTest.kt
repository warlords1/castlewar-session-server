package org.warlords.castlewar.sessionserver.utility

import org.junit.Assert.*
import org.junit.Test
import org.warlords.castlewar.sessionserver.ecs.components.TransformComponent
import kotlin.math.PI

class CollisionMaskTest {

    @Test
    fun circleLineIntersection() {
        val m1 = CircleMask(1.0)
        val m2 = LineSegmentMask(Vector.e)
        val zeroTf = TransformComponent(0.0, 0.0)

        var int = (m1 at zeroTf) intersectionWith (m2 at zeroTf)
        assertNotNull(int)
        assertEquals(0.0, int!!.x, 1e-10)
        assertEquals(0.0, int.y, 1e-10)
        int = (m2 at zeroTf) intersectionWith (m1 at zeroTf)
        assertNotNull(int)
        assertEquals(0.0, int!!.x, 1e-10)
        assertEquals(0.0, int.y, 1e-10)

        var int2 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(-2.0, 0.0))
        assertNotNull(int2)
        assertEquals(-1.0, int2!!.x, 1e-10)
        assertEquals(0.0, int2.y, 1e-10)
        int2 = (m2 at TransformComponent(-2.0, 0.0)) intersectionWith (m1 at zeroTf)
        assertNotNull(int2)
        assertEquals(-1.0, int2!!.x, 1e-10)
        assertEquals(0.0, int2.y, 1e-10)

        var int3 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(0.0, 1.0))
        assertNotNull(int3)
        assertEquals(0.0, int3!!.x, 1e-10)
        assertEquals(1.0, int3.y, 1e-10)
        int3 = (m2 at TransformComponent(0.0, 1.0)) intersectionWith (m1 at zeroTf)
        assertNotNull(int3)
        assertEquals(0.0, int3!!.x, 1e-10)
        assertEquals(1.0, int3.y, 1e-10)

        var int4 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(0.0, 2.0))
        assertNull(int4)
        int4 = (m2 at TransformComponent(0.0, 2.0)) intersectionWith (m1 at zeroTf)
        assertNull(int4)

        var int5 = (m1 at zeroTf) intersectionWith (LineSegmentMask(Vector.zero) at TransformComponent(0.0, 2.0))
        assertNull(int5)
        int5 = (LineSegmentMask(Vector.zero) at TransformComponent(0.0, 2.0)) intersectionWith (m1 at zeroTf)
        assertNull(int5)
    }

    @Test
    fun circleCircleIntersection() {
        val m1 = CircleMask(1.0)
        val m2 = CircleMask(2.0)
        val zeroTf = TransformComponent(0.0, 0.0)

        val int1 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(3.0, 0.0))
        assertNotNull(int1)
        assertEquals(1.0, int1!!.x, 1e-10)
        assertEquals(0.0, int1.y, 1e-10)

        val int2 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(4.0, 0.0))
        assertNull(int2)

        val int3 = (m1 at zeroTf) intersectionWith (m2 at zeroTf)
        assertNotNull(int3)
        assertEquals(0.0, int3!!.x, 1e-10)
        assertEquals(0.0, int3.y, 1e-10)
        assertNull(int2)

        val int4 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(2.5, 0.0))
        assertNotNull(int4)
        assertTrue(int4!!.x > 0.0 && int4.x < 1.0)
        assertEquals(0.0, int4.y, 1e-10)
    }

    @Test
    fun lineLineIntersection() {
        val m1 = LineSegmentMask(Vector.e)
        val m2 = LineSegmentMask(Vector.e.rotated(PI / 2) * 2.0)
        val zeroTf = TransformComponent(0.0, 0.0)

        val int1 = (m1 at zeroTf) intersectionWith (m2 at zeroTf)
        assertNotNull(int1)

        val int2 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(0.5, -1.0))
        assertNotNull(int2)

        val int3 = (m1 at zeroTf) intersectionWith (m1 at zeroTf)
        assertNotNull(int3)

        val int4 = (m1 at zeroTf) intersectionWith (m1 at TransformComponent(2.0, 1.0))
        assertNull(int4)

        val int5 = (m1 at zeroTf) intersectionWith (m2 at TransformComponent(-2.0, -1.0))
        assertNull(int5)
    }

    @Test
    fun weirdMask() {
        val m1 = object : CollisionMask {}
        val zeroTf = TransformComponent(Vector.zero)

        val int = (m1 at zeroTf) intersectionWith (m1 at zeroTf)
        assertNull(int)
    }
}