package org.warlords.castlewar.sessionserver.utility

import org.junit.Test

import org.junit.Assert.*
import kotlin.math.nextDown
import kotlin.math.nextUp

class MathKtTest {

    @Test
    fun constrain() {
        val v1 = constrain(-1.0)
        assertEquals(0.0, v1, 0.0)
        val v2 = constrain(0.0.nextDown())
        assertEquals(0.0, v2, 0.0)
        val v3 = constrain(0.5)
        assertEquals(0.5, v3, 0.0)
        val v4 = constrain(1.0)
        assertEquals(1.0, v4, 0.0)
        val v5 = constrain(1.0.nextUp())
        assertEquals(1.0, v5, 0.0)
        val v6 = constrain(1.5)
        assertEquals(1.0, v6, 0.0)
    }

    @Test
    fun getSq() {
        assertEquals(25.0, 5.0.sq, 0.0)
        assertEquals(25.0, (-5.0).sq, 0.0)
    }

    @Test
    fun round() {
        assertEquals(75.0, 67.0.round(25.0), 1e-10)
        assertEquals(50.0, 62.0.round(25.0), 1e-10)
        assertEquals(50.0, 50.0.round(1.0), 1e-10)
    }
}